from __future__ import division
import numpy as np
import matplotlib as mpl

import matplotlib.pyplot as plt
import scipy.sparse as ss
from bisect import bisect_left
from mpl_toolkits.axes_grid1 import make_axes_locatable

class Grid:
	'''
	Manage landscapes as grids, represented by a sparse matrix of affinities.
	Allows to do plots and to do conversions between the different coordinates of nodes (id in the graph, physical coordinates, grid coordinates)

	columns of the grid are parallels to the y axis of the landscape, and rows to the x axis.
	rows and columns are numbered from the top left corner.
	The order of nodes in the graph correspond to reading the grid from left to right, top to bottom.
	i.e. if there are n columns, the pixel on the ith rows, jth column correspond to the node i*n+j
	All numbering start with 0.

	Physical coordinates take the form (value on the y axis, value on the x axis)
	Grid coordinates take the form (row id, column id)
	TODO: Change the order of these into (x,y)

	Instance variables:
		- A : affinity matrix in the csr format. Great for matrix-vector multiplications
		- A_dok : affinity matrix in the dok format. Access to edge weight in O(1)
		- shape : tuple (number of rows, number of columns). This is the size of the grid representing the landscape
		- map_size : tuple (size along the y axis, size along the x axis). This is the physical
		  size (in meters or kilometers) of the landscape. If None, then self.shape is used.
		- pixel_size : tuple (pixel size along the y axis, pixel size along the x axis)
		- x_ticks : list delimitating the pixels along the x axis (in the physical coordinates). The ith column is delimitated by x_ticks[i] and x_ticks[i+1].
		if there are n colums, len(x_ticks) will be n+1.
		- y_ticks : list delimitating the pixels along the y axis (in the physical coordinates). The ith row is delimitated by y_ticks[i] and y_ticks[i+1].
		if there are n rows, len(y_ticks) will be n+1.
	'''

	def __init__(self, shape, \
		              A=None, \
		              C=None, \
		              map_size=None, \
		              nhood_size=8, \
		              qualities=None, \
		              source_qualities=None, \
		              target_qualities=None, \
		              id_to_grid_coordinate_list=None):
		'''
		Inputs:
			- shape : tuple (number of rows, number of columns). This is the size of the grid representing the landscape
			- A : sparse affinity matrix (if None, A will be set to an unweighted adjacency matrix of a simple grid according to shape)
			- map_size : tuple (size along the y axis, size along the x axis). This is the physical
			  size (in meters or kilometers) of the landscape. If None, then self.shape is used.
			- nhood_size: 4 or 8, corresponding to 'rook' or 'queen' edges, respectively, when A=None
			- qualities : an Nx1 matrix of quality values. If not given, all qualities are set to 1
			- source_qualities &
			  target_qualities : Nx1 matrices of quality values. If either of them is None, that one is set to "qualities"
			- id_to_grid_coordinate_list: in case grid contains empty pixels, this is a list, where element i is the tuple (x,y) of node i's coordinates
		'''
		self.shape = shape
		if C is None:
			self.C = C
		else:
			self.C = ss.csr_matrix(C)
		if map_size is None:
			map_size = shape
		self.map_size = map_size
		self.nhood_size = nhood_size
		self.pixel_size = (map_size[0]/shape[0], map_size[1]/shape[1])
		self.n_rows, self.n_cols = shape
		self.N_grid = shape[0] * shape[1]

		self.x_ticks = np.linspace(0, map_size[1], num=(shape[1]+1))
		self.y_ticks = np.linspace(0, map_size[0], num=(shape[0]+1))

		self.set_id_to_grid_coordinate_list(id_to_grid_coordinate_list)
		# TODO: Fix this in a smarter manner (now id_to_grid_coordinate_list is a list
		# and node_id_to_coordinates is an equivalent method...)

		if A is None:
			self.generate_A()

		if A is not None:
			self.set_A(A)
			self.N = A.shape[0]

		if qualities is not None:
			qualities = np.matrix(qualities)
			self.qualities = np.reshape(qualities, (self.N,1))
		else:
			self.qualities = np.matrix(np.ones(self.N)).T

		if source_qualities is not None:
			source_qualities = np.matrix(source_qualities)
			self.source_qualities = np.reshape(source_qualities, (self.N,1))
		else:
			self.source_qualities = self.qualities

		if target_qualities is not None:
			target_qualities = np.matrix(target_qualities)
			self.target_qualities = np.reshape(target_qualities, (self.N,1))
		else:
			self.target_qualities = self.qualities

	def generate_A(self):
		'''
		Generate the affinity matrix of a grid graph, where each
		pixel is connected to its vertical and horizontal neighbors.

		Parameters:
		- nhood_size: 4 creates horizontal and vertical edges, 8 creates also diagonal edges
		'''
		n_rows = self.n_rows
		n_cols = self.n_cols
		self.N = n_rows*n_cols
		# TODO: WRITE THIS TO ALLOW OTHER VALUES OF nhood_size!

		A = ss.dok_matrix((self.N,self.N))
		for i in xrange(n_rows):
			for j in xrange(n_cols):
				n = i*n_cols + j # current pixel
				if j < n_cols-1:
				# Add horizontal edge:
					A[n, n+1] = 1
				if i < n_rows-1:
				# Add vertical edge:
					A[n, n+n_cols] = 1

					if self.nhood_size==8:
						if j < n_cols-1:
							# Add lower-right diagonal edge:
							A[n, n+n_cols+1] = 1./np.sqrt(2)
						if j > 0:
							# Add lower-left diagonal edge:
							A[n, n+n_cols-1] = 1./np.sqrt(2)
		A = A + A.T         # Symmetrize
		self.set_A(A)

	def set_A(self, A):
		'''
		Set the affinity matrix A and A_dok.
		A should be a sparse matrix.
		'''

		# TODO: CHECK IF THIS IS USELESS:
		# if A.shape[0] != self.N:
		# 	raise ValueError('The shape of the grid does not match the number of nodes of the graph')
		self.A = ss.csr_matrix(A)
		self.A_dok = ss.dok_matrix(A)

	def set_id_to_grid_coordinate_list(self, id_to_grid_coordinate_list):
		if id_to_grid_coordinate_list is None:
			id_to_grid_coordinate_list = []
			for node_id in range(self.N_grid):
				j = int(node_id % self.n_cols)
				i = int((node_id - j) / self.n_cols)
				id_to_grid_coordinate_list.append((i,j))
		self.id_to_grid_coordinate_list = id_to_grid_coordinate_list

	def set_impossible_nodes(self, node_list, impossible_affinity=1e-20):
		'''
		Make pixels impossible to move to by changing the affinities to them to zero.
		Input:
		    - node_list: list of nodes (either node_ids or coordinate-tuples) to be made impossible
		'''
		# If given only one node as a tuple of coordinates:
		if type(node_list) is tuple:
			node_list = [node_list]
		# If given a list of tuples, transform them into node_id's
		if type(node_list[0]) is tuple:
			node_list = [self.grid_coordinates_to_node_id(n) for n in node_list]
		A = self.A.tolil() # Change to lil-format for faster value assignment
		# Set (nonzero) values to impossible_affinity:
		if impossible_affinity > 0:
			A[node_list,:] = impossible_affinity*(A[node_list,:] > 0)
			A[:,node_list] = impossible_affinity*(A[:,node_list] > 0)
		elif impossible_affinity == 0:
			# Delete the nodes completely:
			num_of_removed = len(node_list)

			nodes_to_keep = np.array([n for n in range(self.N) if n not in node_list])

			A = A[nodes_to_keep,:]
			A = A[:,nodes_to_keep]

			# MORE COMPLICATED VERSION OF ABOVE:
			##################
			# sh = A.shape

			# # Delete rows:
			# A.rows = np.delete(A.rows, node_list)
			# A.data = np.delete(A.data, node_list)
			# A._shape = (sh[0]-num_of_removed, sh[1])

			# # Delete columns:
			# A = A.T
			# sh = A.shape
			# A.rows = np.delete(A.rows, node_list)
			# A.data = np.delete(A.data, node_list)
			# A._shape = (sh[0]-num_of_removed, sh[1])
			# A = A.T
			###################

			self.qualities = np.delete(self.qualities, node_list, 0)
			self.source_qualities = np.delete(self.source_qualities, node_list, 0)
			self.target_qualities = np.delete(self.target_qualities, node_list, 0)
			self.id_to_grid_coordinate_list = [self.id_to_grid_coordinate_list[id] for id in range(self.N) if id not in node_list]
			self.N = self.N - num_of_removed

		self.set_A(A)

	def alter_nodes(self, node_list, new_affinity=1e-20, new_quality=1e-20):
		'''
		Alter pixels by changing the affinities and qualities to a new value
		Input:
		    - node_list: list of nodes (either node_ids or coordinate-tuples) to be altered
		'''
		# Save the original affinity and quality values so we can reset:
		if not hasattr(self, 'original_A'):
			self.original_A = self.A.copy()
			self.original_qualities = self.qualities.copy()

		# If given only one node as a tuple of coordinates:
		if type(node_list) is tuple:
			node_list = [node_list]
		# If given only one node_id, transform it into a one-element list:
		elif type(node_list) is int or type(node_list[0]) is float:
			node_list = [node_list]

		# If given a list of tuples, transform them into node_id's
		if type(node_list[0]) is tuple:
			node_list = [self.grid_coordinates_to_node_id(n) for n in node_list]

		if new_affinity is not None:
			# Alter the affinty values
			A = self.A.tolil() # Change to lil-format for faster value assignment
			# Set (nonzero) values to new_affinity:
			A[node_list,:] = new_affinity*(A[node_list,:] > 0)
			A[:,node_list] = new_affinity*(A[:,node_list] > 0)
			self.A = A.tocsr()

		if new_quality is not None:
			# Alter the qualities
			self.qualities[node_list] = new_quality

	def reset_nodes(self):
		self.A = self.original_A
		self.qualities = self.original_qualities

	def node_id_to_grid_coordinates(self, node_id):
		'''
		Return the grid coordinates (row id, column id) corresponding to a given node
		'''
		self.check_node_id(node_id)
		return self.id_to_grid_coordinate_list[node_id]


	def node_id_to_coordinates(self, node_id):
		'''
		Return the physical coordinates of a node (center of the corresponding pixel)
		'''
		return self.grid_coordinates_to_coordinates(self.node_id_to_grid_coordinates(node_id))

	def coordinates_to_grid_coordinates(self, coo):
		'''
		Return the grid coordinates (row id, column id) of the pixel containing the
		physical coordinates "coo" (value along the y axis, value along the x axis)
		'''
		self.check_coordinates(coo)

		i = max(0, bisect_left(self.y_ticks, coo[0]) - 1)
		j = max(0, bisect_left(self.x_ticks, coo[1]) - 1)
		return (i,j)

	def coordinates_to_node_id(self, coo):
		'''
		Return the id of the node containing the physical coordinates "coo"
		the coordinates are interpreted as (value along the y axis, value along the x axis)
		'''
		return self.grid_coordinates_to_node_id(self.coordinates_to_grid_coordinates(coo))

	def grid_coordinates_to_node_id(self, grid_coo):
		'''
		Return the id of the node corresponding to the grid coordinates "grid_coo" (row id, column id)
		'''
		self.check_grid_coordinates(grid_coo)
		#return grid_coo[0] * self.n_cols + grid_coo[1]
		return self.id_to_grid_coordinate_list.index(grid_coo)

	def grid_coordinates_to_coordinates(self, grid_coo):
		'''
		Return the physical coordinates of the center of the pixel identified by "grid_coo" (row id, column id)
		'''
		self.check_grid_coordinates(grid_coo)
		return ((self.y_ticks[grid_coo[0]] + self.y_ticks[grid_coo[0] + 1])/2, (self.x_ticks[grid_coo[1]] + self.x_ticks[grid_coo[1] + 1])/2)

	def check_grid_coordinates(self, grid_coo):
		'''
		Check if grid coordinates are valid. Raises IndexError if not
		'''
		if grid_coo[0] >= self.n_rows or grid_coo[0] < 0:
			raise IndexError('row id outside the range')
		if grid_coo[1] >= self.n_cols or grid_coo[1] < 0:
			raise IndexError('column id outside the range')

	def check_coordinates(self, coo):
		'''
		Check if physical coordinates are valid. Raises IndexError if not
		'''
		if coo[0] > self.map_size[0] or coo[0] < 0:
			raise IndexError('y coordinate outside the range')
		if coo[1] > self.map_size[1] or coo[1] < 0:
			raise IndexError('x coordinate outside the range')

	def check_node_id(self, node_id):
		'''
		Check if node id is valid. Raises IndexError if not
		'''
		if node_id >= self.N or node_id < 0:
			raise IndexError('Node id outside the range')

	def plot_indegrees(self, colormap='cool', cbar_shrink=1., **kwargs):
		indegrees = self.A.transpose().dot(np.ones((self.N, )))
		self.plot(indegrees, colormap=colormap, cbar_shrink=cbar_shrink, **kwargs)

	def plot_outdegrees(self, colormap='cool', cbar_shrink=1., **kwargs):
		outdegrees = self.A.dot(np.ones((self.N, )))
		self.plot(outdegrees, colormap=colormap, cbar_shrink=cbar_shrink, **kwargs)

	def plot(self, values, ax=None, source=None, zeros_to_nan=True, output=None, title=None, colormap='cool', cbar_shrink=1., **kwargs):
		''' if len(values) equals the number of pixels, it plots them as the values of each pixel.
		if len(values) is less than the number of pixels, it tries to interpret them as the ids of the nodes to plot.
		Those nodes will then be given the value 1, while the others will get 0.

		if output is None, the figure is shown, otherwise it is saved in the file named output.
		'''

		new_values = values.copy()
		values = new_values

		canvas = np.zeros(shape=self.shape)

		if self.id_to_grid_coordinate_list is None:
			canvas = np.reshape(values, self.shape)
		else:
			for i,v in enumerate(values):
				coords = self.id_to_grid_coordinate_list[i]
				canvas[coords[0], coords[1]] = v

		if zeros_to_nan:
			canvas[canvas==0] = np.nan

		if len(values) == self.N:
			if ax is None:
				dont_show = False
				fig, ax = plt.subplots()
			else:
				dont_show = True

			img = ax.imshow(canvas, cmap=colormap, **kwargs)
			if source is not None:
				ax.scatter(source[0],source[1], marker='x', s=100)
			ax.axis((-0.5, self.n_cols-0.5, self.n_rows-0.5, -0.5))
			ax.axis('tight')                # Sets axis boundaries based on data (maybe makes above row obsolete)
			ax.axis('off')                  # Remove coordinate axes
			ax.set_aspect('equal', 'datalim')
			cb = self._colorbar(img)

			if title != None:
				ax.set_title(title)

			if output is None and not dont_show:
				plt.show()
			elif output is None and dont_show:
				pass
			else:
				plt.savefig(output)
				plt.close()
		elif len(values) < self.N and max(values) < self.N:
			v = np.zeros((self.N,))
			v[values] = 1
			img, ax, cb = self.plot(v, source=source, zeros_to_nan=zeros_to_nan, ax=ax)
		else:
			raise ValueError('Needs a value for each pixel')

		return img, ax, cb

	def _colorbar(self, mappable):
		# Taken from http://joseph-long.com/writing/colorbars/
		ax = mappable.axes
		fig = ax.figure
		divider = make_axes_locatable(ax)
		cax = divider.append_axes("right", size="5%", pad=0.1)
		return fig.colorbar(mappable, cax=cax)



