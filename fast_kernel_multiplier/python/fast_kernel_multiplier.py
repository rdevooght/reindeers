#!/usr/bin/python2.7
#-*- coding: utf-8 -*-

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy import linalg, optimize, sparse, integrate

def rescale(x, from_range, to_range):
    return (float(x) - from_range[0])/(from_range[1] - from_range[0])*(to_range[1] - to_range[0]) + to_range[0]

def rescale_matrix(A, from_range, to_range):
    epsilon = 0.0001
    scale = (to_range[1] - to_range[0] - epsilon)/(from_range[1] - from_range[0])
    offset = to_range[0] - from_range[0]*(to_range[1] - to_range[0] - epsilon)/(from_range[1] - from_range[0])
    I = sparse.eye(A.get_shape()[0])
    return scale*A + offset*I

def get_range(A):
    # # Get the largest and smallest algebraic eigenvalues of a symmetric matrix A
    # M, vec = sparse.linalg.eigsh(A, k=1, which='LA')
    # m, vec = sparse.linalg.eigsh(A, k=1, which='SA')

    # Eigenvalues are too expensive to compute, lets take a conservative range: the max of row or sum column
    d_col = A.sum(axis=0)
    d_row = A.sum(axis=1)
    l = max([d_col.max(), d_row.max()])
    return [-l, l]

def chebychev_basis(A, v, k):
    # The eigenvalues of A must lie in [-1, 1].
    # If that's not the case, use A = rescale_matrix(A, get_range(A), [-1, 1]) to normalize A
    
    B = np.zeros([len(v), k])
    B[:,0] = v
    B[:,1] = A*v
    
    for i in range(2, k):
        B[:, i] = 2*A*B[:,i-1] - B[:, i-2]
    
    return B

def chebi(x, i):
    # Evaluate the chebyshev polynomial of degree i for x
    if i == 0:
        return 1
    elif i == 1:
        return x
    else:
        s = 1
        t = x
        for j in range(1, i):
            y = 2*x*t - s
            s = t
            t = y
        return y

def chebychev_coefficients(fun, domain, k):
    # Compute the coefficients of the k first chebychev polynomials 
    # for the approximation of function "fun" on the domain "domain"
    
    c = np.zeros(k)
    
    # Projection of rescaled fun on the chebychev basis
    s = lambda x: rescale(x, [-1, 1], domain)
    for i in range(k):
        integrant = lambda x: fun(s(x))*chebi(x, i)/np.sqrt(1 - x**2)
        c[i] = integrate.quad(integrant, -1, 1)[0]
    
    # Normalization
    c = 2*c/np.pi
    c[0] = c[0]/2
    
    return c

def kernel_vector_multiplication(fun, A, v, k):
    # approximate fun(A)*v with a chebychev expansion of degree k
    
    domain = get_range(A)
    A = rescale_matrix(A, domain, [-1, 1]) # rescale A to [-1, 1]
    
    c = chebychev_coefficients(fun, domain, k)
    B = chebychev_basis(A, v, k)
    
    return B.dot(c)
