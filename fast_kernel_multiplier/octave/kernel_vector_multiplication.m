function result = kernel_vector_multiplication(fun, A, v, k)

%range = [eigs(A, 1, 'sa') eigs(A, 1, 'la')];
l = max(max(sum(A, 1)), max(sum(A, 2)));
range = [-l, l];

c = chebychev_coefficients(fun, range, k);
B = chebychev_basis(A, v, k, range);

result = B*c;


function B = chebychev_basis(A, v, k, range)
	epsilon = 0;
	scale = (2-epsilon)/(range(2)-range(1));
	offset = - (1 + 2*range(1)/(range(2)-range(1)));

	A = scale*A + offset*speye(size(A));
	D = diag(sum(A).^(-0.5));
	%A = D*A*D;

	n = size(A, 1);
	B = zeros(n, k);

	B(:, 1) = v;
	B(:, 2) = A*v;
	prev2 = v;
	prev = B(:,2);


	for i = 3:k
		next = 2*A*prev - prev2;
		B(:, i) = next;
		prev2 = prev;
		prev = next;
	    %B(:, i) = 2*A*B(:, i-1) - B(:, i-2);
	end
end

function c = chebychev_coefficients(fun, range, k)

	c = zeros(k, 1);
	a = range(1);
	b = range(2);

	for i = 1:k
	    rescale = @(x) scale(x, [a b], [-1 1]);
	    weighted_cheb = @(x) cheb1(i, rescale(x))./sqrt(1 - rescale(x).^2);
	    c(i) = inner_product(fun, weighted_cheb, a, b);
	end

	%normalization
	c = 4*c/pi/(b-a);
	c(1) = c(1) / 2;
end

function y = cheb1(n, x)
	% Evaluate the chebyshev polynomial (of the first type) of degree n for x

	if n == 1
	    y = ones(size(x));
	elseif n == 2
	    y = x;
	else
	    s = ones(size(x));
	    t = x;
	    for i=3:n
	        y = 2*x.*t - s;
	        s = t;
	        t = y;
	    end
	end
end

function c = inner_product(f1, f2, a, b)

	integrand = @(x, f, g) feval(f, x).*feval(g, x);
	c = quad(@(x) integrand(x, f1, f2), a, b);
end

function y = scale(x, from, to)

	if nargin < 3
	    if length(x) == 1
	        error('scale: for single x, arguments from and to are required')
	    end
	    to = from;
	    from(1) = min(x);
	    from(2) = max(x);
	end

	y = (x - from(1))/(from(2) - from(1))*(to(2) - to(1)) + to(1);
end

end