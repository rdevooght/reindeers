#!/usr/bin/python2.7
#-*- coding: utf-8 -*-

import time
from fast_kernel_multiplier import *

def example_matrix():
    # Build a 5*5 matrix in the CSR format
    # 0 1 2 0 0
    # 1 0 0 0 0
    # 2 0 1 1 0
    # 0 0 1 0 1
    # 0 0 0 1 0
    I = np.array([0, 0, 1, 2, 2, 2, 3, 3, 4])
    J = np.array([1, 2, 0, 0, 2, 3, 2, 4, 3])
    V = np.array([1, 2, 1, 2, 1, 1, 1, 1, 1])
    A = sparse.coo_matrix((V,(I,J)),shape=(5, 5))
    Acsr = sparse.csr_matrix(A, dtype='f')
    return Acsr

def grid(n):
    # build a grid of n*n nodes, where each node is connected to its four neighbours
    I = []
    J = []
    V = []
    for a in range(n):
        for b in range(n):
            i = b*n+a
            top = b*n + ((a-1) % n)
            down = b*n + ((a+1) % n)
            left = ((b-1) % n)*n + a
            right = ((b+1) % n)*n + a
            I.extend([i, i, i, i])
            J.extend([top, down, left, right])
            V.extend([1, 1, 1, 1])
    A = sparse.coo_matrix((V, (I,J)), shape=(n*n, n*n));
    return sparse.csr_matrix(A, dtype='f')

f = lambda x: np.exp(x)
v = np.array([1, 1, 0, 2, 1])
A = example_matrix()
k = 16
print kernel_vector_multiplication(f, A, v, k) # compare that to what you'd get in matlab

# runtime experiment with a grid of 90k nodes
n = 3300
v = np.random.rand(n*n)
A = grid(n)
k = 16
start = time.clock()
r = kernel_vector_multiplication(f, A, v, k)
print 'Elapsed time: ', (time.clock() - start)