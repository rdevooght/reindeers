%f = @(x) exp(x);
f = @(x) 1/(1 - 0.1*x);
k = 16;

v = [1 1 0 2 1]';
%A = example_matrix();
A = rand(5);
kernel_vector_multiplication(f, A, v, k) 

% Should be equal to:
%expm(A)*v
inv(eye(5) - 0.1*A)*v

% % runtime experiment with a grid of 1M nodes
% n = 1000;
% v = rand(n*n, 1);
% tic
% A = grid(n);
% toc
% tic
% r = kernel_vector_multiplication(f, A, v, k);
% toc