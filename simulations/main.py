
import h5py
from scipy import sparse as ss
import numpy as np
import matplotlib.pyplot as plt
from grid_manager import Grid

fpath = '../data/affinity.h5' # Replace with the correct path

plt.ion()

f = h5py.File(fpath)
n_cols = f['NCOLS'][0]
n_rows = f['NROWS'][0]
I = f['AFFINITY_START'][:].tolist()
J = f['AFFINITY_END'][:].tolist()
V = f['AFFINITY_VALUES'][:].tolist()
Q = f['QUALITY_VALUES'][:].tolist()
npix = n_cols*n_rows
# npix = max(max(I),max(J)) + 1 # remove +1 after data bug fix!!!

A = ss.dok_matrix((npix,npix))
C = ss.dok_matrix((npix,npix))

# V = [v-0.9 for v in V] # This will shift the affinity values so that a<1 for unlikely steps

for i,j,v in zip(I,J,V):
    A[i-1,j-1] = v # np.exp(v)
    C[i-1,j-1] = 1/np.exp(v)

A = A.tocsr()
C = C.tocsr()


# Aunw = 1*(A>0)

Asum_in = A.sum(axis=0)
Asum_in = Asum_in.reshape((n_cols,n_rows))
Asum_out = A.sum(axis=1)
Asum_out = Asum_out.reshape((n_cols,n_rows))

plt.figure()
plt.imshow(Asum_in)
plt.colorbar()

plt.figure()
plt.imshow(Asum_out)
plt.colorbar()


Qrect = np.array(Q).reshape(n_cols,n_rows)

plt.figure()
plt.imshow(Qrect)
plt.colorbar()


# G = Grid(graph=)


