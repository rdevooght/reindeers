function A = example_matrix()
	% Build a 5*5 matrix
	% 0 1 2 0 0
	% 1 0 0 0 0
	% 2 0 1 1 0
	% 0 0 1 0 1
	% 0 0 0 1 0
	I = [0, 0, 1, 2, 2, 2, 3, 3, 4];
	J = [1, 2, 0, 0, 2, 3, 2, 4, 3];
	V = [1, 2, 1, 2, 1, 1, 1, 1, 1];
	A = sparse(I+1, J+1, V);
end