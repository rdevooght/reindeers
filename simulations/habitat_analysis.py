from __future__ import division
import numpy as np
import scipy as sp
import random
import matplotlib.pyplot as plt
from math import sqrt
import scipy.sparse as ss
from scipy.sparse.csgraph import dijkstra
from scipy.sparse.csgraph import connected_components
from scipy.stats import rankdata
import warnings
from time import time

from grid_manager import Grid
from landmarks import Landmarks

class HabitatAnalysis:
	def __init__(self, grid, \
				 similarity_measure='RSP_dissimilarity', \
				 compute_KL_divergences=False, \
				 symmetric=False, \
				 beta=None, \
				 landmarks=None, \
				 verbose=True, \
				 algorithm='batch', \
				 linear_solver='direct', \
				 use_bellman_ford=False, \
				 affinity_to_cost='paper_choice', \
				 offset_ac=1, \
				 scale_ac=1, \
				 distance_to_similarity='paper_choice', \
				 offset_dk=1, \
				 scale_dk=1):
		'''
		 - grid is a Grid object representing the landscape

		 - similarity_measure: type of similarity (and distance) to use. Can take several values:
		 	'partition_function' : partition function similarity
		 	'RSP_dissimilarity' : similarity based on expected cost over RSPs
		 	'free_energy' : similarity based on free energy distance
		 	'least_cost' : similarity based on shortest path distance

		 - symmetric: if True, symmetrize the distances, if False, use directed (asymmetric) distances

		 - beta: inverse temperature parameter of the RSP model (unused if similarity_measure='least_cost').

		 - landmarks: a list or array of landmark nodes. If None, all distances are computed, otherwise
		   only distances _to_ landmarks will be computed.

		 - algorithm: If 'batch' (default), compute distances to each landmark at the same time (fast but requires memory)
		 			  If 'sequential', compute distances to each landmark separately (slow but memory-friendly)
		 			  Only applicable when len(landmarks) < N

		 - linear_solver: either 'direct' (default) or 'iterative'

		 - affinity_to_cost: transformation of affinities to costs. Can take several values:
			'minus_log': c_ij = -log(a_ij)
			'minus_log_offset': c_ij = -log(a_ij / offset_ac)
			'minus_log_scale': c_ij = -log(a_ij) / scale_ac
			'equal'
			'inverse': c_ij = 1/a_ij
			'inverse_minus_1': c_ij = 1/a_ij - 0.99
			'one_minus': c_ij = 1-a_ij

			'minus_log_offset_scale'
			'inverse_minus_1_offset'
			'inverse_minus_1_scale'
			'inverse_minus_1_offset_scale'

			'paper_choice' (default)

			or a function handle for a function that defines the transformation of affinities into costs:
			It must accept a numpy vector of floats (the affinities) and return a vector of the same size (the costs)


		 - offset_ac: constant addded to for the 'minus_log_offset' in affinity_to_cost
		 - scale_ac: constant addded to for the 'minus_log_scale' in affinity_to_cost

		 - distance_to_similarity: transformation of distances into similarities. Can take several values:
			'inverse': k_ij = 1/d_ij
			'inverse_of_plus_1': k_ij = 1/(1+d_ij)
			'exp_minus': k_ij = exp(-d_ij)
			'exp_minus_scale': k_ij = exp(-d_ij/scale)
			'hard_cap': k_ij = min(1,1/d_ij)
			'soft_cap': k_ij = 1 - exp(-(1/d_ij))
			'max_minus_distance': k_ij = (max_d - d_ij)/max_d
		 - offset_dk: constant to divide the distance with in 'exp_minus_offset' in distance_to_similarity
		 - scale_dk: constant to divide the distance with in 'exp_minus_scale' in distance_to_similarity
		'''
		self.verbose = verbose
		self.G = grid
		self.N = self.G.N
		self.compute_KL_divergences = compute_KL_divergences

		self.I_W = None
		self.Z = None # Initialize Z

		self.beta = beta
		if beta is None and similarity_measure is not "least_cost":
			self.similarity_measure = "least_cost"
			print "Beta was not given as input; switching to least cost!!!"
		else:
			self.similarity_measure = similarity_measure

		self.symmetric = symmetric
		self.linear_solver = linear_solver
		self.use_bellman_ford = use_bellman_ford
		self.affinity_to_cost = affinity_to_cost
		self.offset_ac = offset_ac
		self.scale_ac = scale_ac
		self.distance_to_similarity = distance_to_similarity
		self.offset_dk = offset_dk
		self.scale_dk = scale_dk

		self.algorithm = algorithm

		if landmarks is None:
			self.landmarks = range(self.N)
			self.n_landmarks = self.N
		else:
			self.landmarks = landmarks
			self.n_landmarks = len(landmarks)

		if self.G.C is None:
			self.compute_cost_matrix()
		else:
			self.C = self.G.C

		# Construct P_ref:
		degrees = self.G.A.sum(axis=1).A.flatten()
		inv_degrees = degrees
		inv_degrees[degrees>0] = 1/inv_degrees[degrees>0]
		D_inv = ss.spdiags(inv_degrees, 0, self.N, self.N)
		P_ref = D_inv.dot(self.G.A)
		self.P_ref = P_ref

		if self.similarity_measure is not 'least_cost':
			self.compute_I_minus_W()

		self.is_symmetric = (self.G.A - self.G.A.T!=0).sum()==0 # Check if affinity matrix is symmetric
		self.n_comps,self.comp_labels = connected_components(self.G.A)

	def set_similarity_measure(self, similarity_measure=None, beta=None):
		'''
		Resets the value of similarity_measure
		'''
		if similarity_measure is None:
			print "You should give new similarity_measure, as one of ['partition_function', 'free_energy', 'least_cost']"
		else:
			if beta is not None:
				self.beta = beta
			self.similarity_measure = similarity_measure
			self.compute_similarities()

	def set_affinity_to_cost(self, affinity_to_cost=None):
		'''
		Resets the value of affinity_to_cost transformation
		'''
		if affinity_to_cost is None:
			print "You should give new affinity_to_cost, as one of ['minus_log' , 'inverse_minus_1'] or a function handle"
		else:
			self.affinity_to_cost = affinity_to_cost
			self.compute_cost_matrix()

	def set_distance_to_similarity(self, distance_to_similarity=None):
		'''
		Resets the value of distance_to_similarity transformation
		'''
		if distance_to_similarity is None:
			print "You should give new distance_to_similarity, as one of ['soft_cap' (default), 'inverse'] or a function handle"
		else:
			self.distance_to_similarity = distance_to_similarity
			self.compute_cost_matrix()

	def alter_nodes(self, node_list, new_affinity=1e-20, new_quality=1e-20):
		'''
		Alter pixels by changing the affinities, costs and qualities to a new value
		Input:
		    - node_list: list of nodes (either node_ids or coordinate-tuples) to be altered
		'''
		if self.C is None:
			# If costs have not been computed, compute them now:
			self.compute_cost_matrix()

		self.G.alter_nodes(node_list, new_affinity=new_affinity, new_quality=new_quality)
		self.compute_cost_matrix()

	def reset_nodes(self):
		'''
		Reset pixel affinities, costs and qualities by reverting the edits made with self.alter_nodes()
		'''
		if not hasattr(self.G, 'A'):
			print "No need to reset, as the grid has not been changed."
		else:
			self.G.reset_nodes()
			self.compute_cost_matrix()


	def compute_cost_matrix(self, min_affinity=0):
		'''
		Compute the cost matrix from the affinity matrix.

		- min_affinity: float (default = 0)
		Affinities below this value will not be considered as possible steps.
		They will be associated with an infinite cost, will have an infinite distance to all landmark,
		and a 0 similarity to all node.
		'''
		A = self.G.A
		C = self.G.A.copy() # make a copy of the affinity matrix

		# Transformation of affinities to cost
		impossible_steps = A.data < min_affinity
		if self.affinity_to_cost == "minus_log": 	# default affinity to cost transformation
			C.data[:] = -np.log(A.data)
		elif self.affinity_to_cost == "equal":
			pass
		elif self.affinity_to_cost == "inverse":
			C.data[:] = 1. / A.data
		elif self.affinity_to_cost == "minus_log_offset":		#I tried to add these
			C.data[:] = -np.log(A.data/self.offset_ac)
			C.data[C.data<0] = 0
		elif self.affinity_to_cost == "minus_log_scale":
			C.data[:] = -np.log(A.data)/self.scale_ac
		elif self.affinity_to_cost == "minus_log_offset_scale":
			C.data[:] = -np.log(A.data/self.offset_ac)/self.scale_ac
			C.data[C.data<0] = 0
		elif self.affinity_to_cost == "inverse_minus_1":
			C.data[:] = 1. / A.data - 0.99      # This scales the costs to ]0, \infty[ when affinities are in [0,1]
		elif self.affinity_to_cost == "inverse_minus_1_offset":			#I tried to add these
			C.data[:] = 1. / A.data - 0.99/self.offset_ac
			C.data[C.data<0] = 0
		elif self.affinity_to_cost == "inverse_minus_1_scale":
			C.data[:] = (1. / A.data - 0.99)/self.scale_ac
		elif self.affinity_to_cost == "inverse_minus_1_offset_scale":
			C.data[:] = (1. / A.data - 0.99/self.offset_ac)/self.scale_ac
			C.data[C.data<0] = 0
		elif self.affinity_to_cost == "one_minus":
			C.data[:] = 1 - A.data
		elif self.affinity_to_cost == "paper_choice":
			C.data[:] = 1. / (2.*A.data) - 1.
			C.data[C.data<0] = 0
		elif type(self.affinity_to_cost) is str:
			raise ValueError('Choice for affinity_to_cost is not valid')
		else:
			C.data[:] = self.affinity_to_cost(A.data) # user-specified affinity to cost transformation
		C.data[impossible_steps] = np.inf
		self.C = C

	def compute_I_minus_W(self, beta=None):
		'''
		Compute the I - W matrix used in the free energy distance.

		Parameters:
		- beta: If given separately, do not change self.I_W but only return I_W according to the given beta.
		        If None, then beta=self.beta and this computes and stores self.I_W

		Returns:
		- I_W: I-W, where I is identity matrix and W is matrix with elements w_ij = P_ij*exp(-beta*c_ij)
		'''
		if beta is None:
			beta = self.beta
		N = self.G.N
		A = self.G.A
		C = self.C
		P_ref = self.P_ref

		# Compute W:
		expbC = -beta*C
		expbC.data = np.exp(expbC.data)
		W = P_ref.multiply(expbC)
		if beta == self.beta:
			self.W = W

		I = ss.eye(N)
		I_W = I-W
		I_W = I_W.tocsc()
		if beta == self.beta:
			self.I_W = I_W

		return I_W

	def compute_similarities(self, min_affinity=0., distance_to_similarity=None):
		'''
		Compute similarities from all nodes to all landmarks and store it
		in self.similarities_all2L (vertical: n_nodes x n_landmarks).

		N.B. : To avoid confusion, cost and affinities are properties of edges, while
		distances and similarities are properties of pair of nodes that are not necessarily directly connected.

		Parameters
		----------
			distance_to_similarity: either one of ['soft_cap' (default), 'inverse'] or a function handle.
		'''

		if self.similarity_measure == "partition_function":
			self.similarities_all2L = self.partition_function_similarities_to(destinations=self.landmarks)
			if self.symmetric:
				self.similarities_all2L = 0.5*(self.similarities_all2L + self.similarities_all2L.T)
			self.distances_all2L = self.similarities_all2L # TODO: This is just a hack to avoid crash in hdf5_reader when saving results...
		else:
			self.compute_distances()
			if self.symmetric:
				self.distances_all2L = 0.5*(self.distances_all2L + self.distances_all2L.T)
			if self.verbose:
				print "Computing distance to similarity transformation"
			self.similarities_all2L = self.compute_distance_to_similarity()


	# TODO: improve this function, maybe separate least_cost and free energy in a better way, allowing other types of distances
	def compute_distances(self):
		'''
		Compute distances from all nodes to all landmarks.
		Store it in self.distances_all2L (vertical: n_nodes x n_landmarks)
		'''
		# From all points to landmarks
		##############################
		if self.verbose:
			print("Compute distances from all nodes to landmarks...")

		if self.similarity_measure == 'least_cost' or np.isinf(self.beta):
			distances_all2L = self.least_cost_distances_to(destinations=self.landmarks)
		elif self.similarity_measure == 'random_walk_cost' or self.beta == 0.:
			distances_all2L = self.random_walk_cost_distances_to(destinations=self.landmarks)
		elif self.similarity_measure == 'free_energy':
			distances_all2L = self.free_energy_distances_to(destinations=self.landmarks)
		elif self.similarity_measure == 'RSP_dissimilarity':
			distances_all2L = self.RSP_dissimilarities_to(destinations=self.landmarks)

		if np.any(distances_all2L<0):
			warnings.warn('Some distances were negative and were set to zero. \
				           This can happen sometimes at least with RSP_dissimilarity \
				           due to numerical inaccuracies (maybe caused by 0-costs?) \
				           and usually the negative values are practically 0', RuntimeWarning)
			distances_all2L[distances_all2L<0] = 0

		self.distances_all2L = distances_all2L


	def least_cost_distances_to(self, destinations=None):
		'''
		Compute least costs from all nodes to a set of destination nodes.

		Parameters:
			compute_KL_divergences: When True, the function outputs also the matrix of KL-divergences between nodes
		'''

		# Shortest-path computation with Dijkstra:
		tic = time()
		if destinations is None:
			destinations = self.landmarks

		if len(destinations) == self.N:
			if self.compute_KL_divergences:
				# To obtain successors of LC_paths_to_destinations, instead of
				# predecessors of LC_paths_from_destinations, C must be transposed:
				D, successors = dijkstra(self.C.T, return_predecessors=True)
				D = D.T
				successors = successors.T
				self.D_KL_LC = self.least_cost_KL_divergences(successors)
			else:
				D = dijkstra(self.C)
		else:
			if self.compute_KL_divergences:
				D, successors = dijkstra(self.C.T, indices=destinations, return_predecessors=True)
				successors = successors.T
				self.D_KL_LC = self.least_cost_KL_divergences(successors, destinations=destinations)
			else:
				D = dijkstra(self.C.transpose(), indices=destinations)
			D = D.T

		if self.verbose:
			toc = time() - tic
			print "Least costs computed in " + str(toc) + " seconds"
		return D

	def least_cost_KL_divergences(self, successors=None, destinations=None):
		'''
		Compute KL-divergences between nodes for the distribution that is 1 for
		one of the least cost paths.
		This corresponds to the limit of RSP KL-divergences only when least cost paths are unique.
		'''
		if destinations is None:
			destinations = self.landmarks
		N = self.N
		N_destinations = len(destinations)

		if successors is None:
			# To obtain successors of LC_paths_to_destinations, instead of
			# predecessors of LC_paths_from_destinations, C must be transposed:
			D, successors = dijkstra(self.C.T, indices=destinations, return_predecessors=True)
			del D
			successors = successors.T

		D_KL_LC = np.zeros((N,N_destinations))

		if N_destinations==N:
			np.fill_diagonal(successors, range(N)) # Set diagonal to 0,1,2,...,N
		else:
			for i_t,t in enumerate(destinations):
				successors[t, i_t] = t

		destinations_matrix, current_succs = np.meshgrid(range(N_destinations), range(N))

		dont_stop = True
		while dont_stop:
			new_succs = successors[current_succs, destinations_matrix]
			p_refs = self.P_ref[current_succs[:], new_succs[:]]
			p_refs.data = -np.log(p_refs.data)
			D_KL_LC += p_refs
			current_succs = new_succs
			dont_stop = p_refs.nnz>0

		return D_KL_LC

	def least_cost_KL_divergences_iterative(self, predecessors=None, destinations=None):
		'''
		Compute KL-divergences between nodes for the distribution that is 1 for
		one of the least cost paths.
		This corresponds to the limit of RSP KL-divergences only when least cost paths are unique.
		'''
		if destinations is None:
			destinations = self.landmarks
		N = self.N
		N_destinations = len(destinations)

		if predecessors is None:
			D, successors = dijkstra(self.C.transpose(), indices=destinations, return_predecessors=True)
			# Matrix of size (N x N_destinations), whose element (s,t)
			# is the next node (after s) on the least cost path from s to t:
			del D
			successors = successors.T

		if N_destinations==N:
			np.fill_diagonal(successors, range(N)) # Set diagonal to 0,1,2,...,N
		else:
			for i_t,t in enumerate(destinations):
				successors[t, i_t] = t

		D_KL_LC = np.zeros((N,N_destinations))
		for i_t,t in enumerate(destinations):
			successors_t = successors[:,i_t]
			current_succs = np.arange(N, dtype=int)
			dont_stop = True
			while dont_stop:
				new_succs = successors_t[current_succs]
				p_refs = self.P_ref[current_succs, new_succs]
				nz_idx = p_refs.nonzero()
				p_refs[nz_idx] = -np.log(p_refs[nz_idx])
				D_KL_LC[:,i_t] += p_refs.A.reshape(N,)
				current_succs = new_succs
				dont_stop = (np.sum(p_refs) > 0)

		return D_KL_LC

	def random_walk_cost_distances_to(self, destinations=None):
		'''
		Compute expected costs over random walks (i.e. beta=0)
		NOTE: This is only implemented for all-to-all distances, and only for undirected graphs
		      We have developed the theory for directed graphs, but that still needs to be implemented and verified.
		'''
		tic = time()
		if destinations is None:
			destinations = self.landmarks

		N = self.N
		if len(destinations) < N:
			raise ValueError("Random walk expected cost computation is (so far) only implemented for all node-pairs")

		if self.is_symmetric:
			A_original = self.G.A
			C_original = self.C
			distances = np.inf*np.ones((N,N))
			for comp_i in range(self.n_comps):
				A = A_original.copy()
				C = C_original.copy()

				remain_idx = np.where(self.comp_labels==comp_i)[0]

				A = A[remain_idx,:]
				A = A[:,remain_idx]
				C = C[remain_idx,:]
				C = C[:,remain_idx]

				N_i = len(remain_idx)

				S = A.sum(axis=1).A.flatten()
				D = ss.spdiags(S,0,N_i,N_i)
				L = D-A

				L = L.todense()

				# Compute pinv(L), pseudoinverse of L using
				# pinv(L) = inv(L + 1/N_i) - 1/N_i
				L = L + 1./N_i
				if self.verbose:
					print "Computing inverse of L - 1/N_i*e*eT ..."
				L = sp.linalg.inv(L)
				L = L - 1./N_i
				# L = sp.linalg.pinv(L) # Pseudoinverse of L

				D_L = L.diagonal()

				b_out = (A.multiply(C)).sum(axis=0).T
				vol = np.sum(b_out)

				# MEMORY-HOG VERSION:
				# D_L_rows = np.repeat(D_L, N_i, axis=0)
				# B = np.repeat(b_out.T, N_i, axis=1)
				# LB = L*B
				# distances = vol*(D_L_rows - L) + LB - LB.T

				L_b = L*b_out
				b_L = b_out.T*L

				L = vol*(D_L - L)
				L = L + L_b
				distances_i = L - b_L

				slice_idx = np.ix_(remain_idx, remain_idx)
				distances[slice_idx] = distances_i.A

			# Set diagonal to zero because of numerical inaccuracies:
			for i in xrange(N):
				distances[i,i] = 0
		else:
			raise ValueError("Random walk expected cost computation is (so far) only implemented for undirected graphs")

		if self.verbose:
			toc = time() - tic
			print "Random walk expected costs computed in " + str(toc) + " seconds"

		return distances

	def free_energy_distances_to(self, destinations=None, use_bellman_ford=None):
		'''
		Compute the Free Energy Distances from all nodes to fixed destination nodes.
		Inputs:
			- destinations: List of destination nodes. If not provided, will be set to self.landmarks.
			- use_bellman_ford: Computes the distances based on the Bellman-Ford iterative algorithm.
			                    Enables computation with large values of beta avoiding infinite values.

		TODO: Implement for from_landmarks_to_all version. Requires computation of the diagonal of Z, which is not trivial...
		TODO: Implement smart way of handling infinite distances, e.g. by relying on free_energy_bellman_ford
		TODO: Check if batch algorithm would be faster if I_dest was dense
		TODO: Implement saving of self.Z for the case N_destinations < N
		'''
		tic = time()
		if destinations is None:
			destinations = self.landmarks

		if use_bellman_ford is None:
			use_bellman_ford = self.use_bellman_ford
		if use_bellman_ford:
			D = self.free_energy_bellman_ford(destinations=destinations)
			return D

		N = self.G.N
		I_W = self.I_W

		N_destinations = len(destinations)

		if N_destinations == self.N:
			# Compute all distances. This requires matrix inversion because of which
			# it's better to use dense instead of sparse matrices
			if self.Z is None:
				I_W = I_W.todense()
				self.Z = sp.linalg.inv(I_W)
			Z = self.Z
			Z_diag_inv = np.diag(1./np.diag(Z))
			Z_h = np.dot(Z,Z_diag_inv)
			D = -1/self.beta * np.log(Z_h)
			for i in xrange(N):
				D[i,i] = 0

		elif self.algorithm is 'batch':
			I_dest = ss.lil_matrix((N,N_destinations))
			I_dest[destinations, range(N_destinations)] = 1
			I_dest = I_dest.tocsc()

			D = self._solve_linear_system(I_W, I_dest)
			D = D.todense().A

			unconnected_pairs = D==0
			if np.any(unconnected_pairs):
				warnings.warn('Some values of D are zero either because of unconnected nodes or a high value of beta, which may lead to infinite distances.', RuntimeWarning)
			D[unconnected_pairs] = 1
			D_dest = D[destinations, range(N_destinations)]
			D = D / D_dest
			D = np.log(D)
			D *= -1./self.beta
			D[unconnected_pairs] = np.inf
			D[destinations, range(N_destinations)] = 0. # Set to zero just to avoid having negative zeros...
			return D

		elif self.algorithm is 'sequential':
			D = np.zeros((N,N_destinations))

			for i,t in enumerate(destinations):
				e_t = np.zeros(N); e_t[t] = 1
				z_t = self._solve_linear_system(I_W, e_t)
				unconnected_nodes = z_t==0

				# If some of the z_t elements are 0, it means beta is too large
				# --> use Bellman-Ford algorithm instead
				if np.any(unconnected_nodes):
					# if use_bellman_ford is False:
					warnings.warn('Some values of z_t are zero either because of unconnected nodes or a high value of beta, which may lead to infinite distances.', RuntimeWarning)
					# else:
					# 	if self.verbose:
					# 		print "Computing FE distances with the Bellman-Ford iteration"
					# 	D = self.free_energy_bellman_ford(destinations=destinations)
					# 	return D

				z_t[unconnected_nodes] = z_t[t] # This will prevent warning from log(0)

				D[:,i] = -1/self.beta * np.log(z_t/z_t[t])
				D[unconnected_nodes,i] = np.inf
		else: raise(ValueError, "algorithm should be one of 'batch'/'sequential'")

		if self.verbose:
			toc = time() - tic
			print "Free energies computed in " + str(toc) + " seconds"

		self.distances_all2L = D

		if self.compute_KL_divergences:
			self.compute_KL_divergences_to(destinations)

		return D

	def RSP_dissimilarities_to(self, destinations=None):
		'''
		Compute RSP expected costs or RSP dissimilarities from all nodes to landmarks.

		TODO: Not yet implemented as from_landmarks_to_all. This requires computation of the diagonal of Z, which is not trivial.
		'''
		tic = time()
		if destinations is None:
			destinations = self.landmarks

		N = self.G.N
		I_W = self.I_W.copy()

		N_destinations = len(destinations)

		CW = self.C.multiply(self.W)

		if len(destinations) == self.N:
			# Compute all distances.
			#
			# TODO: Remove consideration of 0-quality nodes as they may cause unnecessary inf-problems
			#
			if self.Z is None:
				if self.verbose:
					print "Computing Z..."
				I_W = I_W.todense().A
				self.Z = sp.linalg.inv(I_W, overwrite_a=True)
				del I_W

			Z = self.Z

			if np.any(Z==0):
				Z_has_zeros = True
				inf_idx = ss.csr_matrix(Z==0)
				print "Z contains zeros! This will cause some distances to become infinite."
			else:
				Z_has_zeros = False

			if self.verbose:
				print "Computing (C*W)Z..."
			D = CW.todense()
			D = D.A
			D = np.dot(D,Z, out=D)
			if self.verbose:
				print "Computing Z(C*W)Z..."
			D = np.dot(Z,D, out=D)

			if self.verbose:
				print "Computing Z(C*W)Z/Z..."
			D = np.divide(D,Z,out=D)
			# del Z

			if self.verbose:
				print "Computing D - e*diag(D).T"
			if Z_has_zeros:
				D[inf_idx.todense()] = np.inf

			# D above actually gives the expected costs of non-hitting paths
			# from i to j.

			# Expected costs of hitting paths avoiding possible 'Inf-Inf':
			diag_vec = np.diag(D)
			inf_idx = np.isinf(diag_vec)

			# e = np.ones((N,1))
			# diag_D = np.dot(e,diag_vec)          # Matrix whose every row is the diagonal of D (Not needed, because of broadcasting)

			diag_vec = diag_vec[None,:] # Make into a 1xN-vector
			D = D - diag_vec

			D[:,inf_idx] = np.inf

			if self.verbose:
				toc = time() - tic
				print "RSP expected costs computed in " + str(toc) + " seconds"


		elif self.algorithm is 'batch':
			I_dest = ss.lil_matrix((N,N_destinations))
			I_dest[destinations, range(N_destinations)] = 1
			I_dest = I_dest.tocsc()

			Z_dest = self._solve_linear_system(I_W, I_dest)
			Z_dest = Z_dest.todense().A

			unconnected_pairs = Z_dest==0
			if np.any(unconnected_pairs):
				warnings.warn('Some values of Z_dest are zero either because of unconnected nodes or a high value of beta, which may lead to infinite distances.', RuntimeWarning)
			Q_dest = CW*Z_dest

			D = self._solve_linear_system(I_W, Q_dest)

			D = np.divide(D, Z_dest)
			D[unconnected_pairs] = 0
			D_dest = D[destinations, range(N_destinations)]
			D = D - D_dest
			D[unconnected_pairs] = np.inf

		elif self.algorithm is 'sequential':
			D = np.zeros((N,N_destinations))
			for i,t in enumerate(destinations):
				# I_W_t = I_W
				# I_W_t[t,:] = 0  Not really needed

				e_t = np.zeros(N); e_t[t] = 1
				z_t = self._solve_linear_system(I_W, e_t)
				unconnected_nodes = z_t==0
				if any(unconnected_nodes):
					warnings.warn('Some values of z_t are zero either because of unconnected nodes or a high value of beta, which may lead to infinite distances.', RuntimeWarning)
				z_t_2d = np.array([z_t]).T
				q_t = CW*z_t_2d
				c_t = self._solve_linear_system(I_W, q_t)
				c_t = np.divide(c_t, z_t)
				c_t[unconnected_nodes] = 0
				c_t = c_t - c_t[t]

				D[:,i] = c_t
				D[unconnected_nodes,i] = np.inf

		self.distances_all2L = D

		if self.compute_KL_divergences:
			self.compute_KL_divergences_to(destinations)
			# if self.verbose:
			# 	print "Computing KL-divergences"
			# Z_diag_inv = 1./np.diag(Z)
			# D_KL = Z*Z_diag_inv
			# D_KL = -np.log(D_KL) - self.beta*D
			# D_KL[:,inf_idx] = 0

			# # self.mean_D_KL = np.mean(D_KL[D_KL>0])
			# self.mean_D_KL = self.G.source_qualities.T * D_KL * self.G.target_qualities

		return D

	def free_energy_bellman_ford(self, destinations=None, convergence_threshold=1e-6):
		'''
		Computes free energies from all nodes to a set of destination nodes using a
		Bellman-Ford type algorithm employing the log-sum-exp trick for avoiding numerical underflow.
		This is required in some cases, when computing free energies with high values of beta, which may
		cause underflow, resulting in infinite free energies.

		NOTE: THIS IMPLEMENTATION SHOULD BE CHECKED AND VERIFIED! USE AT YOUR OWN RISK
		'''
		if destinations is None:
			destinations = self.landmarks

		N = self.N
		C = self.C

		I_W = self.I_W
		P_ref = self.P_ref

		beta = self.beta

		if type(destinations) is list:
		    N_t = len(destinations)
		else:
		    destinations = [destinations]
		    N_t = 1

		Idx = P_ref > 0 # boolean matrix for non-zero values
		self.Idx = Idx  # store for use for expected costs
		Idx_csr = Idx.tocsr()

		# Initialize expX and expX_idx outside the loop:
		# expX          = ss.lil_matrix((N,N))
		# expX_idx      = ss.lil_matrix((N,N),dtype='bool')
		# expX_idx[Idx] = True

		L = ss.lil_matrix((N,N))
		L[Idx] = -np.log(P_ref[Idx]) + beta*C[Idx] # minus log-likelihood matrix plus cost (term in the exponential)
		L = L.tocsr()

		distances = np.ones((N,N_t))
		log_z_ts  = np.ones((N,N_t))

		for i_t,t in enumerate(destinations):
			d = distances[:,i_t]
			d[t] = 0 # initialization of the distance vector, a column vector

			keep_running = True
			k = 1

			while keep_running & (k < 10000):
				d_old  = d.copy()

				X    = L + (Idx_csr*ss.diags(beta*d))

				# Compute the min above eps of each row
				Xcopy = X.copy()
				shift = np.max(X) + 1.
				Xcopy.data = Xcopy.data - shift
				rowmins = Xcopy.min(axis=1).toarray().flatten() + shift
				rowmins[t] = 0

				D_xmin = ss.diags(rowmins).tocsr()
				X = X - (D_xmin*Idx.tocsr()) # subtract the minimum of each row

				# Update the distance vector d by neglecting contributions < 1e-20:
				expX_idx = Idx.copy().tolil()
				neglectable = ((X >= 20).multiply(Idx))
				expX_idx[neglectable] = False

				# Value lookup is faster with coo-matrices than lil's:
				expX_idx_coo = expX_idx.tocsr().tocoo()
				expX = 1.*expX_idx
				expX[expX_idx_coo] = np.exp(-X[expX_idx_coo])

				total_outflow = expX.sum(axis=1)
				total_outflow = total_outflow.A.flatten()

				log_part = np.log(total_outflow)
				log_part[t] = 0
				log_z_t =  log_part - rowmins
				log_z_t[t] = 0
				d    = -log_z_t/beta
				rel_diff = np.linalg.norm(d - d_old,1)/np.linalg.norm(d_old,1)
				print rel_diff
				keep_running = (rel_diff > convergence_threshold)
				k = k+1

			if k == 10000:
				warnings.warn("The Bellman-Ford algorithm didn't converge", RuntimeWarning)
			log_z_ts[:,i_t] = log_z_t
			distances[:,i_t] = d
		self.log_z_ts = log_z_ts
		return distances

	def expected_cost_bellman_ford(self, destinations=None, convergence_threshold=1e-6):
		'''
		Computes RSP expected costs from all nodes to a set of destination nodes by first computing
		free energies using the method free_energy_bellman_ford.
		This is required in some cases, when computing distances with high values of beta, which may
		cause underflow, resulting in infinite costs.

		NOTE: THIS IMPLEMENTATION SHOULD BE CHECKED AND VERIFIED! USE AT YOUR OWN RISK
		'''
		if destinations is None:
			destinations = self.landmarks

		N = self.N
		if self.C is None:
			self.compute_cost_matrix()
		C = self.C

		if self.I_W is None:
			I_W = self.compute_I_minus_W()
		W = self.W
		P_ref = self.P_ref

		d_FEs = self.free_energy_bellman_ford(destinations=destinations, convergence_threshold=convergence_threshold)
		W.sort_indices()
		N = self.N
		Idx = self.Idx.tocoo()
		P_bias_t_lil = (1.*Idx.copy()).tolil()
		N_t = d_FEs.shape[1] # This should be inferred from somewhere
		distances = np.zeros((N,N_t))
		for i_t,t in enumerate(destinations):
			log_z_t = -d_FEs[:,i_t]*self.beta
			P_bias_t = P_bias_t_lil.copy()

			D_log_z = ss.diags(log_z_t)
			log_z_substraction = Idx*D_log_z - D_log_z*Idx

			P_bias_t[Idx] = np.exp(log_z_substraction[Idx])
			P_bias_t = P_bias_t.tocsr()

			P_bias_t = P_bias_t.multiply(W)
			P_bias_t.data[np.isnan(P_bias_t.data)] = 0
			P_bias_t.eliminate_zeros()

			# Compute expected first passage costs corresponding to P_bias_t:
			# d = np.matrix(np.ones((N,1)))
			# d[t] = 0
			# d = np.matrix(np.zeros((N,1)))
			d = np.matrix(dijkstra(self.C.transpose(), indices=t)).T

			keep_running = True
			k = 1
			while keep_running & (k < 1000):
				d_old = d.copy()
				d = (P_bias_t.multiply(self.C)).sum(axis=1) + P_bias_t*d
				plt.plot(d)
				d[t] = 0
				for i,d_i in enumerate(d):
					if np.isinf(d_i) or np.isnan(d_i):
						print str(i) + str(d_i)
						d[i] = d_old[i] + 1
				print d[2526]
				rel_diff = np.linalg.norm(d - d_old,1)/np.linalg.norm(d_old,1)
				print rel_diff
				if np.isnan(rel_diff):
					print d
					print d_old

				keep_running = (rel_diff > convergence_threshold)
				k = k + 1
			distances[:,i_t] = d.A.flatten()
		return distances

	def compute_KL_divergences_to(self, destinations=None):
		'''
		Computes KL-divergences from all nodes to a set of destination nodes. This is achieved by
		first computing the corresponding RSP free energies and expected costs if they have not
		yet been computed and stored.
		'''
		if destinations is None:
			destinations = self.landmarks

		if np.isinf(self.beta) or self.similarity_measure is "least_cost":
			if self.D_KL_LC is not None:
				print "KL-divergences are already computed!"
			else:
				self.least_cost_KL_divergences(destinations=destinations)

		if hasattr(self, 'distances_all2L'):
			if self.similarity_measure is "free_energy":
				D_FE = self.distances_all2L
				D_EC = self.RSP_dissimilarities_to(destinations=destinations)
			elif self.similarity_measure is "RSP_dissimilarity":
				D_EC = self.distances_all2L
				D_FE = self.free_energy_distances_to(destinations=destinations)
		else:
			D_FE = self.free_energy_distances_to(destinations=destinations)
			D_EC = self.RSP_dissimilarities_to(destinations=destinations)

		self.D_FE = D_FE
		self.D_EC = D_EC
		self.D_KL = self.beta*(D_FE-D_EC)
		mean_D_KL = self.G.source_qualities.T * self.D_KL * self.G.target_qualities
		self.mean_D_KL = mean_D_KL[0,0]
		return self.D_KL


	def compute_distance_to_similarity(self, distance_to_similarity=None):
		'''
		Transforms distances to similarities (=proximities).
		'''
		self.similarities_all2L = np.zeros(self.distances_all2L.shape)
		zero_idx = self.distances_all2L==0
		infi_idx = self.distances_all2L==np.inf
		self.similarities_all2L[zero_idx] = 0 # Self-similarities are set to 0.
		self.similarities_all2L[infi_idx] = 0
		nonzero_noninf = ~(zero_idx | infi_idx)

		if distance_to_similarity is None:
			distance_to_similarity = self.distance_to_similarity

		if distance_to_similarity ==   'inverse':
			self.similarities_all2L[nonzero_noninf] = 1/self.distances_all2L[nonzero_noninf]
		elif distance_to_similarity == 'inverse_of_plus_1':
			self.similarities_all2L[nonzero_noninf] = 1/(self.distances_all2L[nonzero_noninf]+1)
		elif distance_to_similarity == 'inverse_of_plus_1_offset':
			self.similarities_all2L[nonzero_noninf] = 1/(self.distances_all2L[nonzero_noninf]+1)*self.offset_dk
			self.similarities_all2L[self.similarities_all2L>1] = 1
		elif distance_to_similarity == 'inverse_of_plus_1_scale':
			self.similarities_all2L[nonzero_noninf] = 1/(self.distances_all2L[nonzero_noninf]/self.scale_dk+1)
		elif distance_to_similarity == 'inverse_of_plus_1_offset_scale':
			self.similarities_all2L[nonzero_noninf] = 1/(self.distances_all2L[nonzero_noninf]/self.scale_dk+1)*self.offset_dk
			self.similarities_all2L[self.similarities_all2L>1] = 1
		elif distance_to_similarity == 'exp_minus':
			self.similarities_all2L[nonzero_noninf] = np.exp(-self.distances_all2L[nonzero_noninf])
		elif distance_to_similarity == 'exp_minus_offset':
			self.similarities_all2L[nonzero_noninf] = np.exp(-self.distances_all2L[nonzero_noninf])*self.offset_dk
			self.similarities_all2L[self.similarities_all2L>1] = 1
		elif distance_to_similarity == 'exp_minus_scale':
			self.similarities_all2L[nonzero_noninf] = np.exp(-self.distances_all2L[nonzero_noninf]/self.scale_dk)
		elif distance_to_similarity == 'exp_minus_offset_scale':
			self.similarities_all2L[nonzero_noninf] = np.exp(-self.distances_all2L[nonzero_noninf]/self.scale_dk)*self.offset_dk
			self.similarities_all2L[self.similarities_all2L>1] = 1
		elif distance_to_similarity == 'hard_cap':
			self.compute_distance_to_similarity(distance_to_similarity='inverse')
			self.similarities_all2L[self.similarities_all2L>1] = 1
		elif distance_to_similarity == 'soft_cap':
			# k_ij = 1 - exp(-1/d_ij)
			self.similarities_all2L[nonzero_noninf] = 1 - np.exp(-1/self.distances_all2L[nonzero_noninf])
		elif distance_to_similarity == 'max_minus_distance':
			finite_max = np.amax(self.distances_all2L[np.isfinite(self.distances_all2L)]) # Returns the maximum of an array, excluding infinite values
			self.similarities_all2L[nonzero_noninf] = (finite_max - self.distances_all2L[nonzero_noninf])/finite_max
		elif distance_to_similarity == 'paper_choice':
			F = 1./50.
			self.distances_all2L *= F
			self.distances_all2L += 1
			self.similarities_all2L = 1. / self.distances_all2L
			self.distances_all2L -= 1
			self.distances_all2L /= F
		else:
			# user specified distance to similarity transformation:
			self.similarities_all2L = self.distance_to_similarity(self.distances_all2L)

		# Set self-similarities of landmarks to the maximum similarities to them:
		# for i,l in enumerate(self.landmarks):
		# 	self.similarities_all2L[l,i] = np.max(self.similarities_all2L[:,i])
		return self.similarities_all2L

	def partition_function_similarities_to(self, destinations):
		'''
		Computes the hitting path partition function similarities from all nodes to landmarks

		TODO: Implement sequential version, with option of from_landmarks_to_all, which requires computation of diagonal of Z.
		'''
		N = self.G.N
		if self.I_W is None:
			I_W = self.compute_I_minus_W()
		else:
			I_W = self.I_W
		sims = np.zeros((N,len(destinations)))

		for i,t in enumerate(destinations):
			e_t = np.zeros(N); e_t[t] = 1
			if self.linear_solver == "iterative":
				z_t, _ = ss.linalg.bicg(I_W, e_t)
			elif self.linear_solver == "direct":
				z_t = ss.linalg.spsolve(I_W, e_t)
			unconnected_nodes = z_t==0
			z_t[unconnected_nodes] = z_t[t] # This will prevent warning from log(0)

			sims[:,i] = z_t/z_t[t]
			sims[unconnected_nodes,i] = 0

		return sims





	# CENTRALITY & FUNCTIONALITY MEASURES:
	def compute_habitat_functionalities_from_similarities(self):
		'''
		Compute habitat functionalities of all the pixels.
		The habitat functionality of a pixel is an approximation of a weighted harmonic centrality based on Landmarks.
		'''
		normalizers  = np.matrix(np.ones((self.N))).T
		if self.n_landmarks < self.N:
			normalizers *= (self.N-1)/self.n_landmarks # multiply functionalities with (N-1)/N_l
			normalizers[self.landmarks] = (self.N-1)/(self.n_landmarks-1) # multiply functionalities of landmarks with (N-1)/(N_l-1)

		Qs = self.G.source_qualities
		Qt = self.G.target_qualities[self.landmarks]
		K = self.similarities_all2L
		HF = K * Qt
		HF = np.multiply(Qs, HF)

		HF = np.multiply(HF,normalizers)
		# HF[HF==0] = np.nan
		self.habitat_functionalities = HF
		return HF

	def compute_habitat_functionalities_sequentially(self, include_source_qualities=True):
		'''
		Compute habitat functionalities sequentially by first computing proximities to landmarks and
		functionalities from these proximities.
		NOT IMPLEMENTED YET
		'''

	def compute_closeness_functionalities(self, include_source_qualities=False, ranked=False):
		'''
		Compute closeness functionalities of all pixels, i.e. weighted closeness centralities,
		where the distance to node t is weighted by 1-q_t i.e. one minus the quality of node t.
		'''
		normalizers  = np.ones((self.N))
		normalizers *= (self.N-1)/self.n_landmarks # multiply functionalities with (N-1)/N_l
		normalizers[self.landmarks] = (self.N-1)/(self.n_landmarks-1) # multiply functionalities of landmarks with (N-1)/(N_l-1)
		CF = 1/self.distances_all2L.dot(1/self.G.qualities[self.landmarks])
		CF = CF*normalizers
		if include_source_qualities == True:
			CF = self.G.qualities*CF
		# ha[ha==0] = np.nan
		if ranked:
			CF = rankdata(CF)
		return CF

	def RSP_landmark_betweenness(self, beta=None):
		'''
		Compute RSP betweenness of all nodes with respect to paths between all pairs of landmarks
		TODO: Verify that this works
		'''
		if beta is None:
			if not hasattr(self, 'I_W'):
				self.compute_I_minus_W()
			I_W = self.I_W
		else:
			I_W  = self.compute_I_minus_W(beta=beta)

		bet = np.zeros((self.N,)) # Initialize vector of betweennesses

		for s in self.landmarks:
			for t in self.landmarks:
				if s == t: continue
				# Compute betweenness w.r.t. landmarks s and t:
				e_s = self._unit_vector(s)
				e_t = self._unit_vector(t)

				z_s_row = self._solve_linear_system(I_W.T, e_s)
				z_t_row = self._solve_linear_system(I_W.T, e_t)
				z_t_col = self._solve_linear_system(I_W,   e_t)

				# if z_s_row[t]==0 and z_t_row[t]==0:
				# 	bet = z_t_col
				# elif z_t_row[t]==0:
				# 	bet =
				bet_st = (z_s_row/z_s_row[t] - z_t_row/z_t_row[t])*z_t_col

				# The definition is bet_i = sum_j n_ij, so betweenness of t should not be incremented.
				bet_st[s] -= 1
				# bet_st[t] -= 1

				bet = bet + bet_st

		return bet

	def RSP_st_betweenness(self, sources=None, destinations=None, LC_approximation=False):
		'''
		Parameters:

		- LC_approximation: If true, the computation is done by switching P_ref to have uniform values,
		so that the betweenness returned converges to the shortest path betweenness when beta --> Inf
		(instead of the shortest path likelihood betweenness).
		'''
		if sources is None:
			sources = range(self.N)

		if destinations is None:
			destinations = self.landmarks

		if self.I_W is None:
			if self.C is None:
				self.compute_cost_matrix()
			self.compute_I_minus_W()

		I_W = self.I_W.todense().copy()

		if LC_approximation is True:
			P_ref = self.P_ref.copy()
			P_ref.data = np.min(P_ref.data)*np.ones((len(P_ref.data)))

			# Compute W:
			expbC = -self.beta*self.C
			expbC.data = np.exp(expbC.data)
			W = P_ref.multiply(expbC)

			I = ss.eye(self.N)
			I_W = I-W
			I_W = I_W.tocsc()

		bet = np.zeros((self.N,)) # Initialize vector of betweennesses

		for t in destinations:
			I_W_t = I_W.copy()
			I_W_t[t,:] = 0
			I_W_t[t,t] = 1
			for s in sources:
				if s == t: continue
				# Compute betweenness w.r.t. landmarks s and t:
				e_s = self._unit_vector(s)
				e_t = self._unit_vector(t)

				z_s_row = self._solve_linear_system(I_W_t.T, e_s)
				z_t_col = self._solve_linear_system(I_W_t,   e_t)
				# if z_s_row[t]==0 and z_t_row[t]==0:
				# 	bet = z_t_col
				# elif z_t_row[t]==0:
				# 	bet =
				bet_st = (z_s_row/z_s_row[t]) * z_t_col

				# The definition is bet_i = sum_j n_ij, so betweenness of t should not be incremented.
				# bet_st[s] -= 1
				bet_st[t] -= 1

				bet = bet + bet_st


		return bet


	def RSP_full_betweenness(self, LC_approximation=False):
		'''
		Compute full RSP betweenness of all nodes

		Parameters:

		- LC_approximation: If true, the computation is done by switching P_ref to have uniform values,
		so that the betweenness returned converges to the shortest path betweenness when beta --> Inf
		(instead of the shortest path likelihood betweenness).

		TODO: Verify that this works
		'''
		if LC_approximation is True:
			P_ref = self.P_ref.copy()
			P_ref.data = np.min(P_ref.data)*np.ones((len(P_ref.data)))

			# Compute W:
			expbC = -self.beta*self.C
			expbC.data = np.exp(expbC.data)
			W = P_ref.multiply(expbC)

			I = ss.eye(self.N)
			I_W = I-W
			I_W = I_W.tocsc()
		else:
			if self.I_W is None:
				if self.C is None:
					self.compute_cost_matrix()
				self.compute_I_minus_W()
			I_W = self.I_W.todense()


		if LC_approximation is True:
			I_W = 0.000001*(I_W != 0)
			np.fill_diagonal(I_W, 1)

		Z = np.linalg.inv(I_W)
		del I_W
		if Z.min() == 0:
			Z[Z < 1./1000000] = 1./1000000 #used to avoid 0 division
		Zdiv = 1./Z
		D_Zdiv = np.matrix(np.diag(Zdiv)).T

		Zdiv[range(self.N), range(self.N)] -= self.N*D_Zdiv.T

		ZZdiv = Z*Zdiv.T
		return np.sum(np.multiply(ZZdiv, Z.T), axis=1)

	def RSP_full_betweenness_qweighted(self, LC_approximation=False):
		'''
		Compute full RSP betweenness of all nodes weighted by quality
		TODO: Verify that this works
		'''
		if LC_approximation is True:
			P_ref = self.P_ref.copy()
			P_ref.data = np.min(P_ref.data)*np.ones((len(P_ref.data)))

			# Compute W:
			expbC = -self.beta*self.C
			expbC.data = np.exp(expbC.data)
			W = P_ref.multiply(expbC)

			I = ss.eye(self.N)
			I_W = I-W
			I_W = I_W.todense()
		else:
			if self.I_W is None:
				if self.C is None:
					self.compute_cost_matrix()
				self.compute_I_minus_W()
			I_W = self.I_W.copy().todense()

		Z = np.linalg.inv(I_W)
		del I_W
		if Z.min() == 0:
			Z[Z < 1./1000000] = 1./1000000 #used to avoid 0 division
		Zdiv = 1./Z
		D_Zdiv = np.matrix(np.diag(Zdiv)).T

		Qs = self.G.source_qualities
		Qt = self.G.target_qualities
		Qs_sum = np.sum(Qs)


		ZQZdivQ = np.multiply(Qt, Zdiv.T)    # QZdiv
		ZQZdivQ = np.multiply(ZQZdivQ, Qs.T) # QZdivQ
		ZQZdivQ[range(self.N), range(self.N)] -= Qs_sum*np.multiply(Qt, D_Zdiv).T

		ZQZdivQ = Z*ZQZdivQ
		return np.sum(np.multiply(ZQZdivQ, Z.T), axis=1)



	def RSP_full_betweenness_kweighted(self):
		'''
		Compute full RSP betweenness of all nodes weighted with proximity
		'''
		if self.I_W is None:
			if self.C is None:
				self.compute_cost_matrix()
			self.compute_I_minus_W()
		I_W = self.I_W.todense()

		Z = np.linalg.inv(I_W)
		del I_W
		if Z.min() == 0:
			Z[Z < 1./1000000] = 1./1000000 #used to avoid 0 division
		Zdiv = 1./Z

		Qs = self.G.source_qualities
		Qt = self.G.target_qualities
		K  = self.similarities_all2L

		K = np.multiply(Qs, np.multiply(K, Qt.T))

		K_colsum = np.sum(K, axis=0).T
		D_Zdiv = np.matrix(np.diag(Zdiv)).T

		ZKZdiv = np.multiply(K,Zdiv)
		ZKZdiv[range(self.N), range(self.N)] -= np.multiply(K_colsum, D_Zdiv).T

		ZKZdiv = Z*ZKZdiv
		return np.sum(np.multiply(ZKZdiv, Z.T), axis=1)

	def sensitivity(self, include_source_qualities=False, ranked=False, min_affinity=0):
		'''
		The sensitivity of a node is the derivative of the agregated habitat functionality w.r.t. the incomming edges of that node.
		It can be computed only if the free-energy distance is used.
		'''

		if self.distance_measure != 'free_energy':
			print('Warning : the sensitivity is only well defined when distance_measure="free_energy". This is not the case now.')
			return None

		# If needed, do some pre-computations
		if not hasattr(self, '_sum_of_sim') or not hasattr(self, '_sim_all2L') or not hasattr(self, '_sim_L2all'):
			I_W = self.compute_I_minus_W()
			if not hasattr(self, '_sum_of_sim'):
				e = np.ones(self.G.N)
				if self.linear_solver == "iterative":
					self._sum_of_sim, _ = ss.linalg.bicg(I_W.T, e)
				elif self.linear_solver == "direct":
					self._sum_of_sim = ss.linalg.spsolve(I_W.T, e)
			if not hasattr(self, '_sim_all2L'):
				self._sim_all2L = np.zeros((self.G.N,self.n_landmarks))
				for i,t in enumerate(self.landmarks):
					e_t = np.zeros(self.G.N)
					e_t[t] = 1
					if self.linear_solver == "iterative":
						z_t, _ = ss.linalg.bicg(I_W, e_t)
					elif self.linear_solver == "direct":
						z_t = ss.linalg.spsolve(I_W, e_t)
					self._sim_all2L[:,i] = z_t
			if not hasattr(self, '_sim_L2all'):
				self._sim_L2all = np.zeros((self.G.N,self.n_landmarks))
				for i,t in enumerate(self.landmarks):
					e_t = np.zeros(self.G.N)
					e_t[t] = 1
					if self.linear_solver == "iterative":
						z_t, _ = ss.linalg.bicg(I_W.T, e_t)
					elif self.linear_solver == "direct":
						z_t = ss.linalg.spsolve(I_W.T, e_t)
					self._sim_L2all[:,i] = z_t

		# Compute sensitivity
		sensitivity = np.zeros(self.G.N)
		for i,t in enumerate(self.landmarks):
			first_term = (self._sum_of_sim - 1) * self._sim_all2L[:, i] / self._sim_all2L[i,i]
			e_t = np.zeros(self.G.N)
			e_t[t] = 1
			second_term = np.sum(self._sim_all2L[:, i]) * (self._sim_L2all[:,i] - e_t) * self._sim_all2L[:,i] / (self._sim_all2L[i,i] ** 2)
			sensitivity += (first_term - second_term) * self.G.qualities[t]

		sensitivity *= -self.beta

		if include_source_qualities:
			sensitivity *= np.sum(self.G.qualities)
		if ranked:
			sensitivity = rankdata(sensitivity)

		return sensitivity


	def shortest_path_betweenness(self, include_qualities=False, ranked=False):
		'''
		NOT YET IMPLEMENTED
		'''

	def random_walk_betweenness(self, include_qualities=False):
		'''
		NOT YET IMPLEMENTED
		'''

	def landmark_shortest_path_betweenness(self, sources=None, destinations=None, include_qualities=False, ranked=False):
		'''
		Approximation of shortest path betweenness where only the paths between landmarks are considered.

		'''

		if self.similarity_measure != 'least_cost':
			print('Warning : the shortest_path_betweenness is only well defined when distance_measure="least_cost". This is not the case now.')

		if not hasattr(self, 'distances_all2L'):
			self.compute_distances()

		if sources is None:
			sources = self.landmarks

		if destinations is None:
			destinations = self.landmarks

		betweenness = np.zeros((self.G.N,))
		max_betweenness = 0
		for l1 in sources:
			for l2 in destinations:
				if l1 == l2:
					continue
				path_length = self.distances_all2L[l1, :].T + self.distances_all2L[:,l2]
				min_length = np.min(path_length)

				if include_qualities:
					inc = self.G.qualities[l1] * self.G.qualities[l2]
				else:
					inc = 1
				betweenness[np.abs(path_length - min_length) < 1e-5] += inc
				# max_betweenness += inc

		# betweenness /= max_betweenness

		if ranked:
			betweenness = rankdata(betweenness)

		return betweenness


	def distance_hop_relation(self):
		'''
		Plot the evolution of the distance and similarity with the flying distance.

		Take a landmark in the middle of the map as the source node, plot the distance and similarity of all nodes to it.
		'''

		#Find the most central landmark
		central_point = (self.G.n_rows/2, self.G.n_cols/2)
		landmarks_coordinates = map(self.G.node_id_to_grid_coordinates, self.landmarks)
		central_landmark = np.argmin([(i[0] - central_point[0])**2 + (i[1] - central_point[1])**2 for i in landmarks_coordinates])

		# Get flying distance from that landmark to all nodes
		nodes_coordinates = map(self.G.node_id_to_grid_coordinates, range(self.G.N))
		l_coo = landmarks_coordinates[central_landmark]
		flying_distances = [(i[0] - l_coo[0])**2 + (i[1] - l_coo[1])**2 for i in nodes_coordinates]

		# Plot distances
		plt.figure()
		plt.scatter(flying_distances, self.distances_all2L[:, central_landmark])
		plt.title('evolution of distance w.r.t. the flying distance')
		plt.xlabel('Flying distance')
		plt.ylabel('Distance')
		plt.show()

		# Plot similarities
		plt.figure()
		plt.scatter(flying_distances, self.similarities_all2L[:, central_landmark])
		plt.title('evolution of similarity w.r.t. the flying distance')
		plt.xlabel('Flying distance')
		plt.ylabel('Similarity')
		plt.show()

	def plot(self, vec, ax=None):

		# affinities = self.similarities_L2all[landmark_id, :]
		ax = self.G.plot(vec, ax=ax)
		return ax

	# PLOTTING
	def plot_landmark_similarities(self, landmark_id, ax=None):

		# affinities = self.similarities_L2all[landmark_id, :]
		ax = self.G.plot(self.similarities_all2L[:,landmark_id], ax=ax)
		return ax

	def plot_landmark_distances(self, landmark_id, ax=None):

		# affinities = self.similarities_L2all[landmark_id, :]
		ax = self.G.plot(self.distances_all2L[:,landmark_id], ax=ax)
		return ax




	# INTERNAL FUNCTIONS:

	def _solve_linear_system(self, A, x, linear_solver=None):
		if linear_solver == None:
			linear_solver = self.linear_solver
		if linear_solver == "direct":
			return ss.linalg.spsolve(A,x)
		elif linear_solver == "iterative":
			return ss.linalg.bicg(A,x)[0]

	def _unit_vector(self, i, N=None):
		if N is None:
			N=self.N
		e_i = np.zeros((N,1))
		e_i[i] = 1
		return e_i


def new_minus_old_mean_D_KL(beta, *data):
	'''
	Function that computes the difference between a given mean KL-divergence and the
	mean KL-d for a given value of beta. The *arg parameter *data should be a
	3-tuple (G, mean, LM), where
	- G is a Grid object,
	- mean is the sought mean value and
	- LM is a list or array of landmark nodes used for estimating the KL-divergence

	The KL-d estimate is computed as the medina KL-d from all nodes to the landmark nodes
	'''
	beta = beta[0]
	print "Computing with " + str(beta)
	old_mean_D_KL = data[0]
	G = data[1]
	landmarks = data[2]

	landscape = HabitatAnalysis(G, beta = beta, \
		similarity_measure='RSP_dissimilarity', \
		affinity_to_cost='paper_choice',\
		distance_to_similarity='paper_choice',\
		verbose=False)

	N_LM = len(landmarks)

	if N_LM == G.N:
		landscape.RSP_dissimilarities_to(landmarks)
		mean_D_KL = landscape.mean_D_KL
	else:
		D_KL = landscape.compute_KL_divergences_to(destinations=landmarks)
		# D_KL = D_KL[landmarks,:] # CONSIDER ONLY KLD's BETWEEN LANDMARKS
		mean_D_KL = np.mean(D_KL[D_KL>0])

	mean_diff = mean_D_KL-old_mean_D_KL
	return mean_diff

def find_beta_with_D_KL(D_KL, G, lmarks=None, init_beta=0.1):
	if lmarks is None:
		lmarks = np.arange(G.N)
	new_beta = sp.optimize.fsolve(new_minus_old_mean_D_KL, init_beta, xtol=1e-4, args=(D_KL, G, lmarks))[0]
	return new_beta

