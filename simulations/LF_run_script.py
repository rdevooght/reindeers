# import sys
# sys.path.append('../simulations/')
from grid_manager import *
from habitat_analysis import *
from hdf5_reader import *
from time import time
import datetime
import matplotlib.pyplot as plt



data = HDF5Reader('../data/affinity_snohettaV2.h5')
# data = HDF5Reader('../data/affinity_snohetta_small.h5')

g = data.make_grid()

beta = 0.

L_RSP = HabitatAnalysis(g, sampling_method='qualities', \
                        n_landmarks=g.N, \
                        similarity_measure='RSP_dissimilarity', \
                        beta = beta, \
                        affinity_to_cost='paper_choice', \
                        distance_to_similarity='paper_choice')

L_RSP.compute_cost_matrix()

# tic = time()  # For counting the time
L_RSP.compute_similarities()
toc = time() - tic 
print "Time elapsed: " + str(datetime.timedelta(seconds=toc))

print "Computing functionalities and saving results..."
HFs = L_RSP.compute_habitat_functionalities_from_similarities()


LF = np.dot(HFs,L_RSP.G.qualities)

LF_str = 'theta = ' + str(beta) + ', LF = ' + str(LF)     # This is used to print the LF value in the figure (as its title)
image_file_path = 'xxxxx.png'  # Path and filename of figure to save (can be .png or .pdf or pretty much anything)

# With this you can select the colormap:
cax, ax, cb = L_RSP.G.plot(HFs, colormap='cool', title=LF_str, output=image_file_path)

print "Done!"
# toc = time() - tic 
# print "Time elapsed: " + str(datetime.timedelta(seconds=toc))
