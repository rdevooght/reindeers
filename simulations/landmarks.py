from __future__ import division
import numpy as np
import random
import matplotlib.pyplot as plt
from math import sqrt
import scipy.sparse as ss
from grid_manager import Grid
import copy
from scipy.ndimage.filters import gaussian_filter

class Landmarks:
	def __init__(self, grid, n_landmarks=0.01, sampling_method='uniform', pool_size=None):
		'''
		 - grid is a Grid object representing the landscape

		 - n_landmarks is the number of landmark. If it is bigger than 1, it is rounded to the nearest integer, 
		   if it is in (0, 1), it is interpreted as the fraction of nodes to use as landmarks

		 - sampling_method can take several values:
		 	"uniform" (default) : landmarks are spread uniformly on the graph.
		 	"qualities" : landmarks are sampled randomly among the top-quality pixels.
		 	"spread_and_quality" : landmarks are sampled randomly, 
		 		with a bias to cover the whole map and a bias to choose the high quality nodes.
		'''
		self.G = grid
		self.set_n_landmarks(n_landmarks)
		if sampling_method=='qualities':
			self.choose_landmarks_from_qualities(pool_size=pool_size)
		elif sampling_method=='uniform':
			self.choose_landmarks()
		elif sampling_method=='spread_and_quality':
			self.choose_landmarks_spread_and_quality()
		else:
			print ("Invalid option for landmark selection, choosing landmarks uniformly.")
			self.choose_landmarks()


	def set_n_landmarks(self, n_landmarks):
		'''
		Set self.n_landmarks, the number of landmarks to use, based on the parameter n_landmarks

			If n_landmarks it is bigger than 1, it is rounded to the nearest integer, 
			if it is in (0, 1), it is interpreted as the fraction of nodes to use as landmarks
		'''
		N = self.G.N
		if (n_landmarks <= 0 or n_landmarks > N):
			raise ValueError('n_landmarks must be in the interval (0, number of nodes]')
		elif n_landmarks >= 1:
			self.n_landmarks = int(round(n_landmarks))
		else:
			self.n_landmarks = int(round(N * n_landmarks))

	def choose_landmarks(self):
		'''
		Select landmarks uniformly distributed on the grid
		'''

		n_rows, n_cols = self.G.shape
		N = self.G.N

		l_cols = int(max(1, min(self.n_landmarks, round(sqrt(n_cols*self.n_landmarks/n_rows)))))
		
		l_full_rows = int(self.n_landmarks//l_cols)
		n_extra = int(self.n_landmarks - l_cols*l_full_rows)
		
		l_rows = l_full_rows
		if n_extra > 0:
			l_rows += 1
			print('Warning: Landmarks are not uniformly spread. Consider using {} or {} landmarks instead of {}'.format(self.n_landmarks - n_extra, self.n_landmarks - n_extra + l_cols, self.n_landmarks))
		
		rows_indices = [int((2*i+1) * n_rows / l_rows / 2) for i in range(l_rows)]
		col_indices = [int((2*i+1) * n_cols / l_cols / 2) for i in range(l_cols)]
		extra_indices = [int((2*i+1) * n_cols / n_extra / 2) for i in range(n_extra)]
		
		self.landmarks = np.zeros((self.n_landmarks,), dtype=np.int32)
		i = 0
		for r in rows_indices[:l_full_rows]:
			for c in col_indices:
				self.landmarks[i] = self.G.grid_coordinates_to_node_id((r,c))
				i += 1
		for c in extra_indices:
			self.landmarks[i] = self.G.grid_coordinates_to_node_id((rows_indices[-1],c))
			i += 1
	
	def choose_landmarks_from_qualities(self, pool_size=None):
		'''
		Choose landmarks among the top-quality pixels. This avoids using low-quality nodes in the computation of
		the harmonic centrality, as such nodes will have low contribution to the centrality.
		Input:
		    - pool_size: the number of top-quality nodes which the landmarks are sampled from
		      e.g. pool_size = max(np.round(0.5*self.G.N), self.n_landmarks*100)
		'''
		# The number of top nodes among which the landmarks are selected:
		if pool_size is None:
			pool_size = self.n_landmarks
		highest_quality_nodes = np.argsort(self.G.qualities)[-pool_size:][::-1]
		landmarks = random.sample(highest_quality_nodes, self.n_landmarks)
		self.landmarks = sorted(landmarks)

	def choose_landmarks_spread_and_quality(self, width=15, sigma=4):
		''' Choose landmarks based on their quality but tries to spread them all over the map
		At first, the probability of picking a node as landmark is proportional to its quality, but each time a landmark is picked, 
		the probability of picking the pixel surrounding it is lowered. 
		'''

		def make_gaussian_mask(width, sigma):
			'''Generate a mask based on a gaussian filter.
			width defines the width (in number of pixels) of the gaussian filter.
			A large sigma creates a smooth filter, while a small sigma will produce a spike in the middle of the filter.
			'''
			mask = np.zeros((width, width))
			mask[width//2, width//2] = 1
			mask = gaussian_filter(np.float_(mask), sigma)
			mask = 1 - mask / mask[width//2, width//2]
			return mask


		def weighted_choice(weights):
			total = sum(weights)
			r = random.uniform(0, total)
			upto = 0
			i = 0
			for w in weights:
				if upto + w >= r:
					return i
				upto += w
				i+=1
			assert False, "Shouldn't get here"

		def lower_prob(prob, landmark_id, mask):
			'''takes the vector of weights, the id of the new landmark, and a mask (which is an array of factors)
			The mask is applied to the weights, centered on the new landmark
			'''

			mask_size_y = mask.shape[0]
			mask_size_x = mask.shape[1]
			
			(i,j) = self.G.node_id_to_grid_coordinates(landmark_id)
			for x_offset in range(-mask_size_x//2, mask_size_x//2 + 1):
				for y_offset in range(-mask_size_y//2, mask_size_y//2 + 1):
					factor = mask[2 + y_offset][2 + x_offset]
					try:
						node_id = self.G.grid_coordinates_to_node_id((i + y_offset, j + x_offset))
						prob[node_id] *= factor
					except Exception:
						pass

			return prob


		mask = make_gaussian_mask(width, sigma)
		self.landmarks = []
		prob = np.array(self.G.qualities)
		prob *= prob
		for i in range(self.n_landmarks):
			new_landmark = weighted_choice(prob)
			prob = lower_prob(prob, new_landmark, mask)
			self.landmarks.append(new_landmark)



	def plot_landmarks(self):
		''' Plot the position of all landmarks on the grid.
		'''
		values = np.zeros((self.G.N,))
		values[self.landmarks] = 1
		self.G.plot(self.landmarks)

	def plot_landmark(self, landmark_id):
		''' Plot the position of the landmark "landmark_id" on the grid.
		'''
		self.G.plot([self.landmarks[landmark_id]])
