
from gaussian_landscape import *
from grid_manager import *
from landmark_random_walks import *
from hdf5_reader import *

plt.ion()

l = HDF5Reader(fpath='../data/affinity.h5')

g = l.make_grid()
landmarks = Landmarks(g)
landmarks.similarities_to_landmarks(min_affinity=0)

xs = [15,30,45,60]

ys = [10,25,40]

sample_affinities = np.zeros((g.N))
for x in xs:
	for y in ys:
		a_xy = 1./g.A_dok[int(g.grid_coordinates_to_node_id([float(y), float(x)])), :].toarray().ravel()
		a_xy[~np.isfinite(a_xy)] = 0
		sample_affinities += a_xy

g.plot(sample_affinities)


