import h5py
from scipy import sparse
import numpy as np
import matplotlib.pyplot as plt
from grid_manager import Grid

# FPATH = '../data/affinity.h5' # Replace with the correct path
# FPATH = '../r_code/test.h5' # Replace with the correct path
FPATH = '../data/affinity_snohettaV2.h5' # Replace with the correct path

class HDF5Reader:

	def __init__(self, fpath=FPATH):
		self.fpath = fpath

		f = h5py.File(fpath)
		self.nrows = int(f['DIM_ROWCOL'][0])
		self.ncols = int(f['DIM_ROWCOL'][1])
		# self.xllcorner = f['XLLCORNER'][0] #the longitude coordinates of the lower left corner
		# self.yllcorner = f['YLLCORNER'][0] # the latitude coordinates of the lower left corner
		# self.xcellsize = f['XCELLSIZE'][0] # width of the pixels
		# self.ycellsize = f['YCELLSIZE'][0] # height of the pixels (when pixels are square this will be the  same value as the XCELLSIZE, i.e. most cases)
		self.xycellsize = int(f['XYCELLSIZE'][0]) # height of the pixels (when pixels are square this will be the  same value as the XCELLSIZE, i.e. most cases)
		self.epsg = f['EPSG'][0]  # is the code to represent the projection
		self.I = f['AFFINITY_START'][:].tolist()
		self.J = f['AFFINITY_END'][:].tolist()
		self.V = f['AFFINITY_VALUES'][:].tolist()

		self.I = [int(i) for i in self.I]
		self.J = [int(j) for j in self.J]

		# The number of nodes on the graph is determined by the largest indices with affinities:
		self.N = np.max([np.max(self.I), np.max(self.J)]) + 1

		x_coords = [int(c) for c in f['CELL_ROW'][:]]		
		y_coords = [int(c) for c in f['CELL_COL'][:]]


		self.id2coord = zip(x_coords,y_coords)

		Q = f['CELL_VALUES1'][:].tolist()
		self.qualities = np.array(Q)
		# self.nodata_value = f['NODATA_VALUE'][0]  # what represents the missing data in the QUALITY vector
		self.N_grid = self.nrows*self.ncols
		self.nlinks = len(self.V)

	def truncate_affinities(self, min_affinity=-4, max_affinity=4):
		'''Truncate affinity values between min_affinity and max_affinity
		'''
		self.V = np.fmin(max_affinity, self.V)
		self.V = np.fmax(min_affinity, self.V)

	def standardize_affinities(self):
		'''Remove mean and normalize std to 1:
		'''
		meanV = np.mean(self.V)
		stdV = np.std(self.V)
		self.V = (self.V-meanV)/stdV

	def make_grid(self):
		A = sparse.coo_matrix((self.V, (self.I,self.J)), shape=(self.N,self.N)).tocsr() # build a graph with the exponential of the affinities as edge weight
		A.sort_indices
		# map_size = (self.nrows*self.ycellsize, self.ncols*self.xcellsize)
		map_size = (self.nrows*self.xycellsize, self.ncols*self.xycellsize)
		return Grid(map_size=map_size, shape=(self.nrows, self.ncols), A=A, qualities=self.qualities, id_to_grid_coordinate_list=self.id2coord)

	def get_habitat_analysis_group_name(self, ha):
		if ha.similarity_measure == 'partition_function':
			return 'analysis_PF_b' + str(ha.beta) + '_n' + str(ha.n_landmarks)
		elif ha.similarity_measure == 'free_energy':
			return 'analysis_FE_b' + str(ha.beta) + '_n' + str(ha.n_landmarks)
		elif ha.similarity_measure == 'least_cost':
			return 'analysis_LC_n' + str(ha.n_landmarks)
		else:
			raise ValueError('similarity_measure "' + ha.similarity_measure + '" is not implemented')

	def save_habitat_analysis(self, ha):
		'''Save an HabitatAnalysis object, including
		the results of computation:
			- landmarks list
			- distance_all2L
			- sim_all2L
		'''

		group_name = self.get_habitat_analysis_group_name(ha)
		f = h5py.File(self.fpath)
		if group_name in f:
			grp = f[group_name]
		else:
			grp = f.create_group(group_name)

		if 'landmarks' in grp:
			f[group_name]['landmarks'][...] = ha.landmarks
		else:
			l = grp.create_dataset('landmarks', ha.landmarks.shape, dtype=ha.landmarks.dtype)
			l[...] = ha.landmarks

		if 'distances_all2L' in grp:
			f[group_name]['distances_all2L'][...] = ha.distances_all2L
		else:
			l = grp.create_dataset('distances_all2L', ha.distances_all2L.shape, dtype=ha.distances_all2L.dtype)
			l[...] = ha.distances_all2L

		if 'similarities_all2L' in grp:
			f[group_name]['similarities_all2L'][...] = ha.similarities_all2L
		else:
			l = grp.create_dataset('similarities_all2L', ha.similarities_all2L.shape, dtype=ha.similarities_all2L.dtype)
			l[...] = ha.similarities_all2L

	def load_habitat_analysis(self, ha):
		'''If an habitatAnalysis object with the same options is saved in the hdf5,
		load its results (distances and similarities) in the object passed in parameters.
		'''

		if self.is_saved(ha):
			group_name = self.get_habitat_analysis_group_name(ha)
			grp = h5py.File(self.fpath)[group_name]
			ha.landmarks = grp['landmarks'][:]
			ha.distances_all2L = grp['distances_all2L'][:]
			ha.similarities_all2L = grp['similarities_all2L'][:]


	def is_saved(self, ha):
		''' Check if a certain habitat analysis object is already saved in the hdf5
		'''

		group_name = self.get_habitat_analysis_group_name(ha)

		f = h5py.File(self.fpath)
		return (group_name in f)

	def save_habitat_functionality(self, ha, include_source_qualities):
		''' Save the habitat functionality
		'''

		# First save the HabitatAnalysis object (if it is already saved, that won't do anything)
		self.save_habitat_analysis(ha)

		# Compute HF
		HF = ha.compute_habitat_functionalities(include_source_qualities=include_source_qualities, ranked=False)

		# Save habitat functionality
		if include_source_qualities:
			name = 'HF_with_source_quality'
		else:
			name = 'HF_without_source_quality'

		f = h5py.File(self.fpath)
		ha_group = self.get_habitat_analysis_group_name(ha)
		if name not in f[ha_group]:
			hf = f[ha_group].create_dataset(name, HF.shape, dtype=HF.dtype)
		else:
			hf = f[ha_group][name]

		hf[...] = HF