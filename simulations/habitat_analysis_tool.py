import argparse
from habitat_analysis import HabitatAnalysis
from hdf5_reader import *
import matplotlib.pyplot as plt

plt.ion()

def loader(args):
	l = HDF5Reader(fpath=args.filename)
	g = l.make_grid()
	similarity_full_name = {'PF': 'partition_function', 'FE': 'free_energy', 'LC': 'least_cost'}
	landscape = HabitatAnalysis(g, similarity_measure=similarity_full_name[args.similarity_measure],\
		                        beta=float(args.theta), n_landmarks=float(args.landmarks), linear_solver=args.linear_solver,\
		                        affinity_to_cost=args.affinity_to_cost, distance_to_similarity=args.distance_to_similarity)

	if args.no_cache or not l.is_saved(landscape):
		landscape.compute_similarities()
		if not args.no_save:
			l.save_habitat_analysis(landscape)
	else:
		l.load_habitat_analysis(landscape)

	return l, landscape, g


parser = argparse.ArgumentParser()
parser.add_argument('-i', dest='filename', help='map in HDF5 format', required=True)
parser.add_argument('-l', dest='landmarks', help='Number of landmarks (default: 0.01). If between 0 and 1, it is interpreted as the proportion of nodes to be used as landmarks. If bigger than 1, it is interpreted as the number of landmarks.', default=0.01)
parser.add_argument('-s', dest='similarity_measure', choices=['PF', 'FE', 'LC'], help='Similarity measure (default: PF). Use "PF" for the partition function-based similarity, "LC" for least coast (i.e. shortest path) based similarity or "FE" for free energy based similarity', default='PF')
parser.add_argument('-ac', dest='affinity_to_cost', help='affinity_to_cost transformation')
parser.add_argument('-ds', dest='distance_to_similarity', help='distance_to_similarity transformation, should be one of []	')
parser.add_argument('-T', '--theta', help='Parameter of the free energy distance (default: 0.01). Unused with other distance measures.', default=0.01)
parser.add_argument('-S', dest='linear_solver', choices=['direct', 'iterative'], help='The solver used for the linear system appearing in the free energy computation. The direct solver is accurate but slow, whereas the iterative one is fast but inaccurate. Default=\'direct\'', default='direct')
parser.add_argument('--source_quality', help='Use quality of the sources in the habitat functionality value.', action='store_true')
parser.add_argument('--rank', help='Plot the rank of the pixels instead of the values.', action='store_true')
parser.add_argument('--no_cache', help='Don\'t use cached values if they exist.', action='store_true')
parser.add_argument('--no_save', help='Don\'t save the results in the hdf5 file.', action='store_true')
parser.add_argument('--plot', help='Plot the results.', action='store_true')
parser.add_argument('--sens', help='Plot the sensitivity instead of the habitat functionality.', action='store_true')
parser.add_argument('--dist', help='Plot the evolution of distance and similarity w.r.t. to the flying distance.', action='store_true')
parser.add_argument('-o', dest='output_file', help='Save the figure in a file.', default=None)
args = parser.parse_args()

# Load the landscape
hdf5, landscape, grid = loader(args)

# Save habitat functionality in hdf5
if not args.no_save:
	hdf5.save_habitat_functionality(landscape, args.source_quality)

# Plot or save plot of habitat functionality/sensitivity
if args.plot or args.output_file != None:
	if args.sens:
		values = landscape.sensitivity(include_source_qualities=args.source_quality, ranked=args.rank)
		title = "Sensitivity"
	else:
		values = landscape.compute_habitat_functionalities(include_source_qualities=args.source_quality, ranked=args.rank)
		title = "Agregated HF = " + str(np.nansum(values))
	if args.output_file != None:
		grid.plot(values, output=args.output_file, title=title)
	if args.plot:
		grid.plot(values, title=title)

# Plot the evolution of distance
if args.dist:
	landscape.distance_hop_relation()
	k = input("Press enter to quit")
