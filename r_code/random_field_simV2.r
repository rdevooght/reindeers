##"C:\ProgramFiles\R-3.2.2\bin\Rscript.exe" "M:\My Documents\E\Work\Projects\RenewableReindeer\reindeers_git\r_code\random_field_sim.r" lsdim=c(50,100) mu=c(0.5,0.5) sd=c(0.5,0.5) scal=c(10,20) crosscor=0.9 filenam='E:/Work/Projects/RenewableReindeer/sim_dat/affinity_tmp.h5'

comArgs <- commandArgs(T)
eval(parse(text=comArgs))

library(sp)
library(raster)
library(RandomFields)
library(rhdf5)
library(boot)

#test whether the seed argument is provided, if yes use it to set the seed, otherwise, don't
seed <- try(seed, silent=T)
if (class(seed)!="try-error"){set.seed(seed)}
neighdist <- try(neighdist, silent=T)
decayfunct <- try(decayfunct, silent=T)
if (class(neighdist)=="try-error"){neighdist <- 0}
if (class(decayfunct)=="try-error"){decayfunct <- "none"}

simu <- RFsimulate(RMparswmX(nudiag=scal, rho=matrix(nc=2, c(1, crosscor, crosscor, 1))), seq(1,lsdim[1],by=1),seq(1,lsdim[2],by=1), spConform=FALSE)

  myscale <- function(vals, mn, sds){
    vals <- vals/sd(vals)*sds
    vals <- vals - mean(vals) + mn
    vals <- inv.logit(vals)
    return(vals)
  }
  
simu[,,1] <- myscale(simu[,,1], mu[1], sd[1])
simu[,,2] <- myscale(simu[,,2], mu[2], sd[2])
  
res <- list(quality=raster(simu[,,1], xmn=0, xmx=lsdim[2], ymn=0, ymx=lsdim[1]), 
            friction=raster(simu[,,2], xmn=0, xmx=lsdim[2], ymn=0, ymx=lsdim[1]))

#map parameters
h5createFile(filenam)
h5write(res[[1]]@ncols, filenam,"NCOLS") #number of columns.
h5write(res[[1]]@nrows, filenam,"NROWS") #number of rows.
h5write(res[[1]]@extent[1], filenam,"XLLCORNER") #the longitude coordinates of the lower left corner.
h5write(res[[1]]@extent[3], filenam,"YLLCORNER") #the latitude coordinates of the lower left corner.
h5write((res[[1]]@extent[2]-res[[1]]@extent[1])/res[[1]]@ncols, filenam,"XCELLSIZE") #width of the pixels.
h5write((res[[1]]@extent[4]-res[[1]]@extent[3])/res[[1]]@nrows, filenam,"YCELLSIZE") #height of the pixels (when pixels are square this will be the same value as the XCELLSIZE).
h5write(NA, filenam,"EPSG") #is the code to represent the projection.
  
#affinities
yncell <- res[[2]]@ncols*res[[2]]@nrows
if (neighdist==0) {
  adj <- as.data.frame(adjacent(res[[2]], c(1:yncell)))}

if (neighdist>0) {
  neighmat <- matrix(1, ncol=(neighdist*2)+1, nrow=(neighdist*2)+1)
  neighmat[neighdist+1, neighdist+1] <- 0
  adj <- as.data.frame(adjacent(res[[2]], c(1:yncell), directions=neighmat))
  
  if (decayfunct!="none"){
    adj$dist <- sqrt((xFromCell(res[[2]], adj$from)-xFromCell(res[[2]], adj$to))^2+(yFromCell(res[[2]], adj$from)-yFromCell(res[[2]], adj$to))^2)
  }
}

if (decayfunct=="lin"){
  adj$tdist <- abs(adj$dist - max(adj$dist))
  adj$tdist <- adj$tdist/max(adj$tdist)
  #plot(adj$dist, adj$tdist, ylim=c(0,1))
}

if (decayfunct=="exp"){
  lambda <- log(2)/(max(adj$dist)/8)
  adj$tdist <- exp(-lambda*(adj$dist-1))
  #plot(adj$dist, adj$tdist, ylim=c(0,1))
}

if (decayfunct=="none"){adj$affinity <- values(res[[2]])[adj$to]}
if (decayfunct!="none"){adj$affinity <- values(res[[2]])[adj$to]*adj$tdist}

h5write(adj$from, filenam,"AFFINITY_START") #vector of start pixels. Pixels are numbered from the upper left corner (1) to the lower right corner (N); as we read in English. 
h5write(adj$to, filenam,"AFFINITY_END") #vector of end pixels, same numbering as the start pixels.
h5write(adj$affinity, filenam,"AFFINITY_VALUES") #vector with the affinity value between the start and the end pixel. Only none-zero values are represented.

#qualities
h5write(NA, filenam,"NODATA_VALUE") #what represents the missing data in the QUALITY_VALUES vector.
h5write(values(res[[1]]), filenam,"QUALITY_VALUES") #vector with the habitat quality values (from the upper left corner to the lower right corner, as we read in English).
