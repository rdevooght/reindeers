from __future__ import division
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal
from scipy import sparse
from grid_manager import Grid

class GaussianLandscape:
	
	def __init__(self, map_size=(100,100), positive_gaussian=3, negative_gaussian=3, rescale_factor=2.):
		self.map_size = map_size
		self.min_edge_weight = 1e-5
		self.min = None
		self.max = None
		self.rescale_factor = rescale_factor
		self.nbr_of_gaussians = (positive_gaussian, negative_gaussian)
		self.generate_landscape()
		self.make_qualities() # This should be written in a smarter way...
	
	def generate_landscape(self):
		
		def random_gaussian(xmax, ymax):
			mean = np.random.rand(1,2)*[xmax, ymax]
			cov1 = np.random.rand(1).tolist()[0]*xmax*10
			cov2 = np.random.rand(1).tolist()[0]*ymax*10
			cov12 = (np.random.rand(1).tolist()[0]*2 - 1)*xmax/10
			cov = [[cov1, cov12], [cov12, cov2]]
			return multivariate_normal(mean.tolist()[0], cov)
		
		self.positive_gaussians = [random_gaussian(self.map_size[0], self.map_size[1]) for i in range(self.nbr_of_gaussians[0])]
		self.negative_gaussians = [random_gaussian(self.map_size[0], self.map_size[1]) for i in range(self.nbr_of_gaussians[1])]
	
	def get_min(self):
		if self.min == None:
			pos = np.dstack(np.mgrid[0:self.map_size[0], 0:self.map_size[1]])
			z = self(pos, rescale=False)
			self.min = np.min(z)
		return self.min

	def get_max(self):
		if self.max == None:
			pos = np.dstack(np.mgrid[0:self.map_size[0], 0:self.map_size[1]])
			z = self(pos, rescale=False)
			self.max = np.max(z)
		return self.max

	def rescale(self, values):
		# a,b = [-1, 1]
		# values = (b - a) * (values - self.get_min()) / (self.get_max() - self.get_min()) - a
		# values = 1. / (1  + np.exp( -2* values))
		
		# a = self.rescale_factor / self.get_max()
		# values = np.maximum(0, values - self.get_min())
		# values = 1 - np.exp(-1 * a * values)

		a,b = [self.min_edge_weight, 1]
		values = (b - a) * (values - self.get_min()) / (self.get_max() - self.get_min()) - a

		return values
	
	def __call__(self, coordinates, rescale=True):
		positive_part = np.sum([g.pdf(coordinates) for g in self.positive_gaussians], axis=0)
		negative_part = np.sum([g.pdf(coordinates) for g in self.negative_gaussians], axis=0)
		if rescale:
			return self.rescale(positive_part - negative_part)
		else:
			return positive_part - negative_part
	
	def plot(self, pixel_size=1):
		x, y = np.mgrid[0:self.map_size[0]:pixel_size, 0:self.map_size[1]:pixel_size]
		pos = np.dstack((x, y))
		z = self(pos, rescale=True)
		plt.figure()
		# plt.pcolor(x, y, z, cmap='RdYlGn')
		plt.imshow(z,cmap='RdYlGn')
		plt.colorbar()
		plt.show()

	def edge_weight(self, G, i, j, weight='destination'):
		i_coo = G.node_id_to_coordinates(i)
		i_abstract_coo = G.node_id_to_grid_coordinates(i)
		j_coo = G.node_id_to_coordinates(j)
		j_abstract_coo = G.node_id_to_grid_coordinates(j)
		if np.sum(np.abs(np.array(i_abstract_coo) - np.array(j_abstract_coo))) != 1:
			return 0
		else:
			if weight == 'destination':
				return self(j_coo)
			elif weight == 'average':
				return (self(i_coo) + self(j_coo))/2.0
	
	
	def make_grid(self, pixel_size=1):
		n_rows = int(np.round(self.map_size[0] / pixel_size))
		n_cols = int(np.round(self.map_size[1] / pixel_size))
		G = Grid(self.map_size, (n_rows, n_cols))


		I = []
		J = []
		Z = []
		for a in range(n_rows):
			for b in range(n_cols):
				i = a*n_cols+b
				top = a*n_cols + ((b-1) % n_cols)
				down = a*n_cols + ((b+1) % n_cols)
				left = ((a-1) % n_rows)*n_cols + b
				right = ((a+1) % n_rows)*n_cols + b
				I.extend([i, i, i, i])
				J.extend([top, down, left, right])
				Z.extend([self.edge_weight(G, i, j) for j in [top, down, left, right]])
		A = sparse.coo_matrix((Z, (I,J)), shape=(n_rows*n_cols, n_rows*n_cols))
		min_edge = np.amin(A.data)
		if min_edge < self.min_edge_weight:
			A.data[:] = A.data - min_edge + self.min_edge_weight
		G.set_affinities(A)
		return G

	def make_qualities(self,pixel_size=1):
		x, y = np.mgrid[0:self.map_size[0]:pixel_size, 0:self.map_size[1]:pixel_size]
		pos = np.dstack((x, y))
		z = self(pos, rescale=True)
		# Use landscape to generate qualities by doing some manipulation
		# This should be improved with more time...
		z_new = z + 0.3*z[::-1,::-1] + 0.3*z[:,::-1] + 0.3*z[::-1,:]
		z_new /= z_new.max() # Normalize to [0,1]
		self.qualities = z_new.flatten()

	def plot_qualities(self,pixel_size=1):
		n_rows = int(np.round(self.map_size[0] / pixel_size))
		n_cols = int(np.round(self.map_size[1] / pixel_size))
		qualities_grid = self.qualities.reshape((n_rows, n_cols))
		plt.figure()
		plt.imshow(qualities_grid,cmap='RdYlGn')
		plt.colorbar()
		plt.show()


	# def jump_weight(self, i, j):
	# 	_, i_abstract_coo = self.node_id_to_coordinates(i)
	# 	_, j_abstract_coo = self.node_id_to_coordinates(j)
	# 	ia, ib = i_abstract_coo
	# 	ja, jb = j_abstract_coo
	# 	n = len(self.x_ticks)
	# 	w = 1
	# 	if ia == ja:
	# 		for s in range(np.abs(ib - jb)):
	# 			if ib < jb:
	# 				w *= self.edge_weight(ia + (ib+s)*n, ia + (ib+s+1)*n)
	# 			else: 
	# 				w *= self.edge_weight(ia + (ib-s)*n, ia + (ib-s-1)*n)
	# 	elif ib == jb:
	# 		for s in range(np.abs(ia - ja)):
	# 			if ia < ja:
	# 				w *= self.edge_weight(ia+s + ib*n, ia+s+1 + ib*n)
	# 			else: 
	# 				w *= self.edge_weight(ia-s + ib*n, ia-s-1 + ib*n)
	# 	else:
	# 		w = 0
	# 	return w

	# def add_jumps(self, jump=10):
	# 	n = len(self.x_ticks)
	# 	m = len(self.y_ticks)
	# 	I = []
	# 	J = []
	# 	Z = []
	# 	for a in range(jump, n-jump):
	# 		for b in range(jump, m-jump):
	# 			i = b*n+a
	# 			top = b*n + (a-jump)
	# 			down = b*n + (a+jump)
	# 			left = (b-jump)*n + a
	# 			right = (b+jump)*n + a
	# 			I.extend([i, i, i, i])
	# 			J.extend([top, down, left, right])
	# 			Z.extend([self.jump_weight(i, j) for j in [top, down, left, right]])
	# 	J = sparse.coo_matrix((Z, (I,J)), shape=(n*m, n*m));
	# 	self.A = self.A+J.tocsr()