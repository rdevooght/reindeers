function A = grid(n)
	% build a grid of n*n nodes, where each node is connected to its four neighbours
	edges = zeros(4*n*n, 2);
	j = 1;
	for a = 0:n-1
		for b = 0:n-1
			i = b*n+a;
			top = b*n + mod(a-1, n);
			down = b*n + mod(a+1, n);
			left = mod(b-1,n)*n + a;
			right = mod(b+1,n)*n + a;
			edges(j:j+3, :) = [i top; i down; i left; i right];
			j = j+4;
		end
	end

	A = sparse(edges(:,1) + 1, edges(:,2) + 1, ones(length(edges), 1));
end