import numpy as np
from scipy import sparse

'''
3*2 grid
edge weight determined by the destination:
    |1|2|
    |3|1|
    |1|4|
'''
I = [0, 0, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 5, 5]
J = [1, 2, 0, 3, 0, 3, 4, 1, 2, 5, 2, 5, 3, 4]
V = [2., 3., 1., 1., 1., 1., 1., 2., 3., 4., 3., 4., 1., 1.]
V = [v/5 for v in V]
G = sparse.coo_matrix((V,(I,J)),shape=(6, 6), dtype=np.float32)



%matplotlib qt5
from matplotlib import pyplot as plt
from matplotlib import gridspec

import sys
sys.path.append('./simulations/')

import networkx as nx
from grid_manager import Grid
import operator
from habitat_analysis import HabitatAnalysis

grid_shape = [5,5]
n_rows,n_cols = grid_shape
N = n_rows*n_cols

qualities = np.ones((N,))
g = Grid(shape=grid_shape, qualities=qualities)


# beta = 1.
a_to_c = 'equal' # affinity_to_cost
d_to_k = 'inverse' # distance_to_similarity


betas = [0.001, 0.002, 0.005, 0.01, 0.05, 0.1, 1, 10]
# nbetas = len(betas)

landscapes_RSP = []
for beta in betas:
	landscape = HabitatAnalysis(g, sampling_method='qualities', n_landmarks=N,\
	                            similarity_measure='RSP_dissimilarity', matrix_computation=False, beta=beta, affinity_to_cost=a_to_c,\
	                            distance_to_similarity=d_to_k)
	landscape.compute_similarities()
	landscapes_RSP.append(landscape)



fig, axs = plt.subplots(2,4,figsize=(18,10))
fig.suptitle('Sums of RSP distances with varying betas', fontsize=20)
for landscape, beta, ax in zip(landscapes_RSP, betas, axs.flatten()):
    sum_of_dists_from = np.sum(landscape.distances_all2L,axis=1)
    g.plot(sum_of_dists_from, ax=ax, title='beta = ' + str(beta))
