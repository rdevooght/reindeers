from gaussian_landscape import *
from grid_manager import *
from habitat_analysis import *
from hdf5_reader import *

l = HDF5Reader(FPATH)
g = l.make_grid()

import time

start = time.time()

landscape_RSP = HabitatAnalysis(g, sampling_method='qualities', n_landmarks=g.N, similarity_measure='least_cost',matrix_computation=True, beta=0.1, affinity_to_cost='paper_choice')

landscape_RSP.compute_similarities()

end = time.time()
print(end - start)









plt.ion()

print("Do you want to generate a landscape or read it from a file ?\n"
		"'g' to generate a landscape,\n"
		"'f' to read it from a file")
k = raw_input()

if k == 'g':
	# Generate landscape until users finds a good one
	while True:
		l = GaussianLandscape(positive_gaussian=5, negative_gaussian=7)
		l.plot()

		print("Do you want to keep this landscape ? y/[n]")
		k = raw_input()
		if k == 'y':
			break
		else:
			plt.close()
else:
	# l = HDF5Reader(fpath='../data/affinity.h5')
	l = HDF5Reader(fpath='../r_code/test01.h5')
	# l.standardize_affinities()
	# l.truncate_affinities()

g = l.make_grid()

print("How should landmarks be chosen?\n"
		"'q' = based on qualities,\n"
		"'s' = spread and based on qualities,\n"
		"'u' = uniformly")

k = raw_input()

if k == 'q':
	criterion = 'qualities'
elif k == 'u':
	criterion = 'uniform'
elif k == 's':
	criterion = 'spread_and_quality'
else:
	print "Invalid choice, using uniform."

print("How many landmarks?")
n_landmarks = int(raw_input())


print("Which distance measure should be used?\n"
		"'lc' = least cost distance,\n"
		"'fe' = free energy distance")

k = raw_input()

if k == 'lc':
	distance_measure='least_cost'
	beta=None
elif k == 'fe':
	distance_measure='free_energy'
	print("Set value of beta")
	beta = float(raw_input())
else:
	print "Invalid choice, using least cost distance."
	distance_measure='least_cost'
	beta=None


landscape = HabitatAnalysis(g,sampling_method=criterion,n_landmarks=n_landmarks, distance_measure=distance_measure, beta=beta)
landscape.compute_distances()

while True:
	print("What do you want to do?\n"
		   "'q' to print landscape qualities,\n"
		   "'id' to print indegrees,\n"
		   "'od' to print outdegrees,\n"
		   "'l' to plot the position of all landmarks,\n"
		   "'lq' to plot the position of all landmarks on the quality map,\n"
		   "'b' to plot the shortest-path betweenness of all points,\n"
		   "'hf' to plot habitat functionalities of all points,\n"
		   "'s' to plot sensitivity")
	k = raw_input()
	if k == 'q':
		g.plot(l.qualities)
	elif k == 'id':
		g.plot_indegrees()
	elif k == 'od':
		g.plot_outdegrees()
	elif k == 'ad':
		g.plot(g.A.mean(axis=1))
	elif k == 'l':
		g.plot(landscape.landmarks)
	elif k == 'lq':
		q = np.copy(l.qualities)
		q[landscape.landmarks] = np.nan
		g.plot(q)

	elif k == 'edges':
		print("Give x coordinate: ")
		x = raw_input()
		print("Give y coordinate: ")
		y = raw_input()
		node_id = int(g.grid_coordinates_to_node_id([float(y), float(x)]))

		print node_id
		g.plot(g.A_dok[node_id, :].toarray().ravel(), source=(x,y))

	elif k == 'hf':
		k_old = k
		print("Would you like to\n"
			  "'i' include qualities of the source node, or\n"
			  "'e' exclude qualities of the source node")
		k = raw_input()
		include_source_qualities = k=='i'

		print("Would you like to plot\n"
			  "'a' actual values, or\n"
			  "'r' ranks of pixels")
		k = raw_input()
		ranked = k=='r'

		g.plot(landscape.habitat_functionalities(include_source_qualities=include_source_qualities, ranked=ranked))

	elif k == 's':

		g.plot(landscape.sensitivity())
		
	elif k == 'b':
		print("Would you like to\n"
			  "'i' include qualities, or\n"
			  "'e' exclude qualities")
		k = raw_input()
		include_qualities = k=='i'

		print("Would you like to plot\n"
			  "'a' actual values, or\n"
			  "'r' ranks of pixels")
		k = raw_input()
		ranked = k=='r'

		g.plot(landscape.shortest_path_betweenness(include_qualities=include_qualities, ranked=ranked))

	elif k == 'lmL':
		print("Give landmark id: ")
		l_id = raw_input()
		g.plot([landscape.landmarks[int(l_id)]])
		g.plot(landscape.similarities_all2L[:, int(l_id)])
		# plt.figure()
		# plt.plot(landscape.similarities_L2all[int(l_id), :])
		plt.show()
	elif k == 'lmC':
		dist = landscape.distances_all2L
		landmark_centralities = dist.dot(np.ones((dist.shape[1],)))
		plt.figure()
		plt.plot(landmark_centralities)
		plt.show()